Drupal.behaviors.JHU = {
  attach: function (context) { 

    jQuery('#mass-contact-mail-page #mails .form-checkbox').click(function() {
      var emails = '';

      jQuery('#mass-contact-mail-page #mails .form-checkbox').each(function(){
        if (this.checked == true) {
          emails += jQuery(this).next().text().trim() + ';';
        }
      });
    
      jQuery('#edit-tomail').val(emails);
    });

    jQuery("#edit-mails-all").click(function() {
      var checkedValue = jQuery('#edit-mails-all').attr('checked') ? 'checked' : '';
      jQuery('#mass-contact-mail-page #mails .form-checkbox').attr('checked', checkedValue);
      var emails = '';

      jQuery('#mass-contact-mail-page #mails .form-checkbox').each(function(){
        if (this.checked == true) {
          emails += jQuery(this).next().text().trim() + ';';
        }
      });
    
      jQuery('#edit-tomail').val(emails);
    });
  }
}