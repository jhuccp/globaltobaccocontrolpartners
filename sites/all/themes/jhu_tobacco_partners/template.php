<?php

/**
 * @file
 * This file is empty by default because the base theme chain (Alpha & Omega) provides
 * all the basic functionality. However, in case you wish to customize the output that Drupal
 * generates through Alpha & Omega this file is a good place to do so.
 *
 * Alpha comes with a neat solution for keeping this file as clean as possible while the code
 * for your subtheme grows. Please read the README.txt in the /preprocess and /process subfolders
 * for more information on this topic.
 */

/**
 * Implements hook_preprocess_html()
 */
function jhu_tobacco_partners_preprocess_html(&$vars) {
  if (arg(0) == 'comment' && arg(1) == 'reply' && $parent_node = menu_get_object('node', 2)) {
    $vars['classes_array'][] = 'comment-' . $parent_node->type;
    $vars['attributes_array']['class'][] = 'comment-' . $parent_node->type;
  }
}

/**
 * Implements hook_preprocess_page()
 */
function jhu_tobacco_partners_preprocess_page(&$vars) {
  if (drupal_lookup_path('alias', current_path()) == 'old-interface' && user_is_logged_in()) {
    $vars['page']['content']['content']['content']['system_main']['nodes'] = array();

    // Set links for Frontpage block titles.
    // Upcoming events.
    if (isset($vars['page']['content']['content']['content']['views_upcoming_events-block'])) {
      $vars['page']['content']['content']['content']['views_upcoming_events-block']['#block']->subject =
        l(t('Upcoming Calendar Events'), 'event-search', array('attributes' => array('class' => array('block-title-link'))));
    }
    // All Partners Message Board
    if (isset($vars['page']['content']['content']['content']['views_98aaedbcb99cf47d247a62155ac8d28a'])) {
      $vars['page']['content']['content']['content']['views_98aaedbcb99cf47d247a62155ac8d28a']['#block']->subject =
        l($vars['page']['content']['content']['content']['views_98aaedbcb99cf47d247a62155ac8d28a']['#block']->subject, 'forum/100', array('attributes' => array('class' => array('block-title-link'))));
    }
    // Latest Library Resources
    if (isset($vars['page']['content']['content']['content']['views_c9ad55dc98b6fdf8d6deb52533974cf1'])) {
      $vars['page']['content']['content']['content']['views_c9ad55dc98b6fdf8d6deb52533974cf1']['#block']->subject =
        l($vars['page']['content']['content']['content']['views_c9ad55dc98b6fdf8d6deb52533974cf1']['#block']->subject, 'library_documents', array('attributes' => array('class' => array('block-title-link'))));
    }
  }

  if ((drupal_is_front_page() || drupal_lookup_path('alias', current_path()) == 'old-interface') && user_is_logged_in()) {
    // Set the page title.
    global $user;
    $account = user_load($user->uid);
    drupal_set_title(t('Welcome Back, @name', array('@name' => $account->field_user_firstname[LANGUAGE_NONE][0]['value'])));
  }

  // Provide homepage icons for logged in users
  if (drupal_is_front_page() && user_is_logged_in()) {
    $nids = array_keys($vars['page']['content']['content']['content']['system_main']['nodes']);
    $vars['page']['content']['content']['content']['system_main']['nodes'][$nids[0]]['body'][0]['#markup'] = "<table class='fronttable'>
  <th colspan='3'><a href='node/add/library'><img class='upload' src='/sites/all/themes/tobacco7/images/upload.png' /></a><a href='/library_documents'> <img class='viewlibrary' src='/sites/all/themes/tobacco7/images/viewlibrary.png' /></a>
  </th>
  <tr>
    <td class='tdemail'><a href='/sendmail'><img  class='email' src='/sites/all/themes/tobacco7/images/email.png' /></a></td>
    <td class='tdcalendaradd'><a href='/node/add/event'><img class='calendaradd' src='/sites/all/themes/tobacco7/images/addcalendar.png' /></a></td>
    <td class='tdcalendar'><a href='/calendar/month'><img class='calendar' src='/sites/all/themes/tobacco7/images/calendar.png' /></a></td>
  </tr>
  <tr>
    <td colspan='3'>
<a href='/old-interface'><img class='revert' src='/sites/all/themes/tobacco7/images/revert.png'/></a>
</td>
  </tr>
</table>";
  }

  if (arg(0) == 'search') {
    $vars['tabs'] = array();
  }
  if (!empty($vars['node']) && $vars['node']->type == 'forum') {
    $forum = taxonomy_term_load($vars['node']->forum_tid);
    $vars['title'] = t('Forum for !forum', array('!forum' => $forum->name));
  }

  if (arg(0) == 'mass_contact') {
    $vars['title'] = t('Mailing Lists');
  }

  // Country page right sidebar
  if (isset($vars['node']) && $vars['node']->type == 'country') {
    $vars['page']['content']['content']['content']['system_main']['#prefix'] = '<div class="content-leftside">';
    $vars['page']['content']['content']['content']['block_11']['#prefix'] = '</div><div class="content-rightside">';
    $vars['page']['content']['content']['content']['views_country_latest_from_forum-block']['#suffix'] = '</div>';
  }
}

/**
 * Implements hook_form_alter()
 */
function jhu_tobacco_partners_form_alter(&$form, &$form_state) {
  if ($form['#id'] == 'search-block-form') {
    unset($form['search_block_form']['#title_display']);
  }

  if ($form['#id'] == 'views-exposed-form-event-search-page') {
    $form['#after_build'] = array('jhu_tobacco_partners_event_search_page_after_build');
    $form['region']['#options']['All'] = $form['country']['#options']['All'] =
    $form['type']['#options']['All'] = $form['topic']['#options']['All'] =
    $form['audience']['#options']['All'] = $form['partner_organization']['#options']['All'] = t('All');
  }

  // Set 'All' instead of '-Any-' default value for Search Library block.
  if ($form['#id'] == 'views-exposed-form-library-search-page') {
    $form['region']['#options']['All'] = $form['country']['#options']['All'] =
    $form['type']['#options']['All'] = $form['topic']['#options']['All'] =
    $form['language']['#options']['All'] = t('All');
  }

  if ($form['#id'] == 'jhu-event-date-timezone-switcher') {
    $form['#prefix'] = '<div class="desc">' . t('To see the Start and End times for another location, select the location from the list and the times will change automatically. In order to go back to the default timezone select "Default" from the dropdown list') . '</div>';
    if (arg(0) == 'print') {
      $form = array();
    }
  }

  if ($form['#id'] == 'user-login-form') {
    $form['hr'] = array(
      '#markup' => '<hr />',
      '#weight' => 99,
    );
    $form['actions']['#weight'] = 98;
    $form['links']['#weight'] = 100;
  }

  if ($form['#id'] == 'search-form') {
    $form['basic']['keys']['#title_display'] = 'invisible';
  }
}

/**
 * After_build callback for Event page form
 */
function jhu_tobacco_partners_event_search_page_after_build(&$form, &$form_state) {
  $form['start_date']['value']['date']['#description'] = '';
  $form['end_date']['value']['date']['#description'] = '';

  return $form;
}

function jhu_tobacco_partners_library_search_form_submit(&$form, &$form_state) {
  if (!empty($form_state['values']['type']['All'])) {
    $options = $form['type']['#options'];
    unset($options['All']);
    $form_state['values']['type'] = $options;
  }
}

/**
 * Implements hook_preprocess_breadcrumb().
 *
 * Add node link to breadcrumb for all nodes.
 */
function jhu_tobacco_partners_preprocess_breadcrumb(&$vars) {
  $arg_1 = arg(1);
  $arg_2 = arg(2);
  if (arg(0) == 'node' && is_numeric($arg_1) && empty($arg_2)) {
    $node = node_load($arg_1);
    // We have special case for Country nodes.
    if ($node->type == 'country') {
      $vars['breadcrumb'][] = l(t('Country Overview for @country', array('@country' => $node->title)), 'node/' . $node->nid);
    }
    else {
      $vars['breadcrumb'][] = l($node->title, 'node/' . $node->nid);
    }
  }

  // Set breadcrumb for frontpage.
  if (drupal_is_front_page()) {
    if (user_is_logged_in()) {
      $vars['breadcrumb'] = array(
        l(t('Home'), '<front>'),
      );
    } else {
      $vars['breadcrumb'] = array();
    }
  }
}

/**
 * Implements hook_preprocess_field().
 */
function jhu_tobacco_partners_preprocess_field(&$variables, $hook) {
  $element = $variables['element'];

  if ($element['#field_name'] == 'field_user_lastname') {
    $variables['label'] = t('Name');
    $variables['items'][0]['#markup'] = $element['#object']->field_user_firstname[LANGUAGE_NONE][0]['safe_value'] . ' ' . 
                                        $element['#object']->field_user_lastname[LANGUAGE_NONE][0]['safe_value'];
  }
  else if (in_array($element['#field_name'], array('field_library_partner', 'field_library_topic', 'field_library_region', 'field_library_country', 'field_event_partner', 'field_event_topic', 'field_event_region', 'field_event_country'))) {
    $terms = array();
    foreach($element['#items'] as $item) {
      $terms[] = $item['taxonomy_term']->name;
    }
    $variables['items'] = array(array('#markup' => join(', ', $terms)));
  }
  else if ($element['#field_name'] == 'body' && isset($element['#object']) && $element['#object']->type == 'library') {
    $variables['label'] = t('Description');
  }
  // Alter Library fields.
  else if ($element['#field_name'] == 'field_library_submittedby') {
    $variables['label'] = t('Uploaded by (Partner)');
  }
  else if ($element['#field_name'] == 'field_event_topic') {
    $terms = array();
    foreach($element['#items'] as $item) {
      $terms[] = $item['taxonomy_term']->name;
    }
    $variables['items'] = array(array('#markup' => join('; ', $terms)));
  }
}

/**
 * Implements hook_preprocess_page()
 */
function jhu_tobacco_partners_preprocess_node(&$vars) {
  if ($vars['node']->type == 'library') {
    $vars['content']['field_title'] = array(
      '#markup' => '<div class="field"><div class="field-label">' . t('Title of Document') . ':&nbsp;</div><div class="field-items"><div class="field-item even">' . $vars['node']->title . '</div></div></div>',
    );
    // Add 'Date and Time Uploaded' field.
    $vars['content']['created'] = array(
      '#theme' => 'field',
      '#title' => t('Date and Time Uploaded'),
      '#label_display' => 'above',
      '#field_type' => 'text',
      '#field_name' => 'created',
      '#bundle' => 'library',
      '#object' => $vars['node'],
      '#items' => array(),
      '#entity_type' => 'node',
      '#weight' => 4,
      '#items' => array(
        0 => '&nbsp;',
      ),
      0 => array('#markup' => format_date($vars['node']->created, 'custom', 'F j, Y \a\t g:i a')),
    );
  }

  if (isset($vars['node']->type) && $vars['node']->type == 'event') {
    $vars['content']['field_title'] = array(
      '#markup' => '<div class="field"><div class="field-label">' . t('Event Title') . ':&nbsp;</div><div class="field-items"><div class="field-item even">' . $vars['node']->title . '</div></div></div>',
    );

    $vars['title'] = 'Calendar';
  }

  if (isset($vars['node']->type) && $vars['node']->type  == 'library') {
    $vars['title'] = t('Library');
  }
  if ($vars['node']->type == 'forum') {
    $author = user_load($vars['node']->uid);
    $vars['posts'] = t('Posts: !count', array('!count' => '1234'));
    $vars['joined'] = t('Joined: !join', array('!join' => format_date($author->created)));
    $vars['page'] = FALSE;
    if (!empty($vars['content']['links']['comment']['#links']['comment-add'])) {
      $reply = $vars['content']['links']['comment']['#links']['comment-add'];
      $vars['reply'] = l($reply['title'], $reply['href'], $reply);
    }
  }
}

function jhu_tobacco_partners_preprocess_search_result(&$vars) {
  $vars['info'] = $vars['result']['link'];
}

function jhu_tobacco_partners_preprocess_search_results(&$vars) {
  global $pager_page_array, $pager_total, $pager_total_items, $pager_limits;

  $current_page = $pager_page_array[0];
  $total_pages = $pager_total[0];
  $total_items = $pager_total_items[0];
  $pager_limit = $pager_limits[0];

  $start_item = $current_page * $pager_limit + 1;
  $end_item = ($current_page + 1) * $pager_limit;
  if ($current_page + 1 == $total_pages) {
    $end_item = $total_items;
  }
  $vars['search_navigation'] = t('[@start-@end <span>of</span> @total]', array('@start' => $start_item, '@end' => $end_item, '@total' => $total_items));
}

function jhu_tobacco_partners_preprocess_comment(&$vars) {
  if ($vars['node']->type == 'forum') {
    $author = user_load($vars['comment']->uid);
    $vars['posts'] = t('Posts: !count', array('!count' => '1234'));
    $vars['joined'] = t('Joined: !join', array('!join' => format_date($author->created)));
    $vars['title'] = $vars['comment']->subject;
    $vars['permalink'] = l('#' . $vars['id'], 'node/' . $vars['node']->nid, $vars['comment']->uri['options']);
  }
}

/**
 * Preprocess for views-view-field.tpl.php.
 */
function jhu_tobacco_partners_preprocess_views_view_field(&$vars) {
  if ($vars['field']->field == 'field_event_audience' && empty($vars['output'])) {
    $vars['output'] = t('none');
  }

  // Show end date on Search Event view even if end date
  // is the same as start date.
  if ($vars['field']->field == 'field_event_date') {
    $output = str_replace(' to ', '&nbsp;-&nbsp;', strip_tags($vars['output']));
    if (strlen($output) < 15) {
      $output .= '&nbsp;-&nbsp;' . $output;
    }
    $vars['output'] = $output;
  }
}
