<div class="forum-reply"><?php print $reply; ?></div>

<article<?php print $attributes; ?>>
  <?php print $user_picture; ?>
  
  <?php if (!$page && $title): ?>
  <header>
    <?php print render($title_prefix); ?>
    <h2<?php print $title_attributes; ?>><a href="<?php print $node_url ?>" title="<?php print $title ?>"><?php print $title ?></a></h2>
    <?php print render($title_suffix); ?>
  </header>
  <?php endif; ?>
  
  <?php if ($display_submitted): ?>
  <footer class="submitted"><?php print $date; ?></footer>
  <?php endif; ?>  

  <div class="author">
    <p class="name"><?php print $name; ?></p>
    <p class="posts"><?php print $posts; ?></p>
    <p class="joined"><?php print $joined; ?></p>
  </div>
    
  <h3 class="topic-title"><?php print $title ?></h3>

  <div<?php print $content_attributes; ?>>
    <?php
      // We hide the comments and links now so that we can render them later.
      hide($content['comments']);
      hide($content['links']);
      print render($content);
    ?>
  </div>
  
  <div class="clearfix">
    <?php if (!empty($content['links'])): ?>
      <nav class="links node-links clearfix"><?php print render($content['links']); ?></nav>
    <?php endif; ?>

    <?php print render($content['comments']); ?>
   
  </div>
</article>

<div class="forum-reply"><?php print $reply; ?></div>
