<?php

/**
 * Implements hook_process_region().
 *
 * Set custom titles.
 */
function jhu_tobacco_partners_alpha_process_region(&$vars) {
  $arg2 = arg(2);
  if ((arg(0) == 'user') && $vars['region'] == 'content' && empty($arg2)) {
    $user = user_load(arg(1));
    if (isset($user->field_user_partnerorganization[LANGUAGE_NONE][0]['value'])) {
      $vars['title'] = t('Partners: @partner', array('@partner' => $user->field_user_partnerorganization[LANGUAGE_NONE][0]['value']));
    }
  }

  if (arg(0) == 'node' && $vars['region'] == 'content' && empty($arg2)) {
    $node = node_load(arg(1));

    // Add 'Workspace of' to titles of workspace and topic_workspace nodes.
    if (isset($node->type) && ($node->type == 'workspace' || $node->type == 'topic_workspace')) {
      $vars['title'] = t('Work Space for @title', array('@title' => $node->title));
    }

    // Calendar node page title
    if (isset($node->type) && $node->type == 'event') {
      $vars['title'] = t('Calendar');
    }

    // Library node page title
    if (isset($node->type) && $node->type == 'library') {
      $vars['title'] = t('Library');
    }

    // Country node page title
    if (isset($node->type) && $node->type == 'country') {
      $vars['title'] = t('Country Overview for @country', array('@country' => $node->title));
    }
  }
}