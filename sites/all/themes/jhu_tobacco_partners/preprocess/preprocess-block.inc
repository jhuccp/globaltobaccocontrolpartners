<?php

/**
 * Implements hook_preprocess_block().
 *
 * Add links to titles of the blocks.
 */
function jhu_tobacco_partners_alpha_preprocess_block(&$vars) {
  // Library Documents titles should be linked to library search page
  $library_blocks = array(
    'library_documents-block' => array(322,323,324,325,326),
    'library_documents-block_1' => array(332),
    'library_documents-block_2' => array(334),
    'library_documents-block_3' => array(87),
  );

  if (in_array($vars['block']->delta, array_keys($library_blocks))) {
    switch($vars['block']->delta) {
      case 'library_documents-block':
      case 'library_documents-block_1':
      case 'library_documents-block_2':
        $type = '&type[]=' . join('&type[]=', $library_blocks[$vars['block']->delta]);
        $args = '?keys=&region=All&country=All' . $type . '&topic=All&language=All';
        break;
      case 'library_documents-block_3':
        $arg = current($library_blocks[$vars['block']->delta]);
        $args = '?keys=&region=All&country=' . $arg . '&type=All&topic=All&language=All';
        break;
    }

    $vars['title_prefix']['library_search'] = array(
      '#markup' => '<a href="' . base_path() .'library-search' . $args . '" class="block-title-link">',
    );

    $vars['title_suffix']['library_search'] = array(
      '#markup' => '</a>',
      '#weight' => -1,
    );
  }

  // Add links to titles on Workspaces  and Topic Workspace pages.
  $workspace_blocks = array(
    'workspace_calendar_events-block',
//    'workspace_calendar_events-block_1',
    '53fa91ee9c3da7644cabd91c43557b39', // Should be workspace_calendar_events-block_1 but somehow views shows this hash.
    'workspace_latest_forums-block',
    'workspace_documents-block',
    'workspace_documents-block_1',
    // Country Latest Library Resources.
    '8284f148053ff6bd5dce07b703e80b6e',
    // Upcoming Calendar Events.
    '6af5d11c9383c022bddcafef802dc971',
    // Country: Latest from Forums
    'country_latest_from_forum-block',
    // Tasks block.
    'workspace_task_block-block',
  );
  if (in_array($vars['block']->delta, $workspace_blocks)) {
    $link = '';
    $workspace_node = node_load(arg(1));
    if (!$workspace_node) {
      return;
    }

    switch ($vars['block']->delta) {
      case 'workspace_calendar_events-block':
        // Upcoming Calendar Events
        if (!isset($workspace_node->field_workspace_country[LANGUAGE_NONE][0]['tid'])) {
          return;
        }
        $link = 'event-search?keys=&region=All&country='
        . $workspace_node->field_workspace_country[LANGUAGE_NONE][0]['tid']
        . '&type=All&topic=All&audience=All&partner_organization=All&start_date'
        . '[value][date]=&end_date[value][date]=';
        break;
      case 'workspace_calendar_events-block_1':
      case '53fa91ee9c3da7644cabd91c43557b39':
        // Latest Documents.
        if (!isset($workspace_node->field_topic_workspace_topic[LANGUAGE_NONE][0]['tid'])) {
          return;
        }
        $link = 'event-search?keys=&region=All&country=All&type=All&topic='
        . $workspace_node->field_topic_workspace_topic[LANGUAGE_NONE][0]['tid']
        . '&audience=All&partner_organization=All&start_date'
        . '[value][date]=&end_date[value][date]=';
        break;
      case 'workspace_latest_forums-block':
        // Latest from the Forums.
        if (isset($workspace_node->field_workspace_forums[LANGUAGE_NONE][0]['tid'])) {
          $link = 'forum/' . $workspace_node->field_workspace_forums[LANGUAGE_NONE][0]['tid'];
        }
        if (isset($workspace_node->field_topic_workspace_forum[LANGUAGE_NONE][0]['tid'])) {
          $link = 'forum/' . $workspace_node->field_topic_workspace_forum[LANGUAGE_NONE][0]['tid'];
        }
        break;
      case 'workspace_task_block-block':
        // Latest from the Forums.
        if (isset($workspace_node->field_workspace_forums[LANGUAGE_NONE][0]['tid'])) {
          $link = 'task/' . $workspace_node->field_workspace_forums[LANGUAGE_NONE][0]['tid'];
        }
        if (isset($workspace_node->field_topic_workspace_forum[LANGUAGE_NONE][0]['tid'])) {
          $link = 'task/' . $workspace_node->field_topic_workspace_forum[LANGUAGE_NONE][0]['tid'];
        }
        break;
      case 'workspace_documents-block':
        // Latest Documents.
        if (!isset($workspace_node->field_workspace_country[LANGUAGE_NONE][0]['tid'])) {
          return;
        }
        $link = 'library-search?keys=&region=All&country='
        . $workspace_node->field_workspace_country[LANGUAGE_NONE][0]['tid']
        . '&type=All&topic=All&language=All';
        break;
      case 'workspace_documents-block_1':
        // Latest Documents.
        if (!isset($workspace_node->field_topic_workspace_topic[LANGUAGE_NONE][0]['tid'])) {
          return;
        }
        $link = 'library-search?keys=&region=All&country=All&type=All&topic='
        . $workspace_node->field_topic_workspace_topic[LANGUAGE_NONE][0]['tid']
        . '&language=All';
        break;
      case '6af5d11c9383c022bddcafef802dc971':
        // Upcoming Calendar Events.
        if (!isset($workspace_node->field_country_country[LANGUAGE_NONE][0]['tid'])) {
          return;
        }
        $link = 'event-search?keys=&region=All&country='
          . $workspace_node->field_country_country[LANGUAGE_NONE][0]['tid']
          . '&type=All&topic=All&audience=All&partner_organization=All&'
          . 'start_date[value][date]=' . format_date(REQUEST_TIME, 'custom', 'Y-m-d') . '&end_date[value][date]=';
        break;
      case '8284f148053ff6bd5dce07b703e80b6e':
        // Country Latest Library Resources.
        if (!isset($workspace_node->field_country_country[LANGUAGE_NONE][0]['tid'])) {
          return;
        }
        $link = 'library-search?keys=&region=All&country='
          . $workspace_node->field_country_country[LANGUAGE_NONE][0]['tid']
          . '&type=All&topic=All&language=All';
        break;
      case 'country_latest_from_forum-block':
        // Country: Latest from Forums
        if (isset($workspace_node->field_country_forum[LANGUAGE_NONE][0]['tid'])) {
          $link = 'forum/' . $workspace_node->field_country_forum[LANGUAGE_NONE][0]['tid'];
        }
        break;
    }

    if (!empty($link)) {
      $vars['title_prefix']['library_search'] = array(
        '#markup' => '<a href="' . base_path() . $link . '" class="block-title-link">',
      );

      $vars['title_suffix']['library_search'] = array(
        '#markup' => '</a>',
        '#weight' => -1,
      );
    }
  }

  // Coutry: Other Countries Overview (Map).
  if ($vars['block']->delta == '11') {
    $vars['block']->subject = t('Other Countries Overview');
  }

  // Coutry: Country Statistics.
  if ($vars['block']->delta == 'country_statistics-block') {
    $country_node = node_load(arg(1));
    $vars['block']->subject = t('@country Statistics', array('@country' => $country_node->title));
  }
}
