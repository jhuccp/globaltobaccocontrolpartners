<?php

/**
 * Implements hook_preprocess_calendar_item().
 *
 * Add colors to Calendar items.
 */
function jhu_tobacco_partners_alpha_preprocess_calendar_item(&$vars) {
  $color_index = &drupal_static('color_index');

  if (!$color_index || $color_index > 10) {
    $color_index = 0;
  }

  if (isset($vars['item']) && count($vars['item'])) {
//    $vars['item']->class .= 'item color-' . ++$color_index;
  }
}
