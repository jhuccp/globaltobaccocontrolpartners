3.1.0 (Brainy Betty)
f31fb2ee1fb30a5430ebb2e71e058557e2b599a0
o:Sass::Tree::RootNode
:@has_childrenT:@template"#	/**
 * @file
 * Tabs Styling
 *
 * Adds styles for the primary and secondary tabs.
 *
 * Compare this with default CSS found in the system module's stylesheet (a copy
 * of which is in drupal7-reference.css.)
 */

@import "base";


// Some variables to make altering the styling easier.
$tabs-container-bg: #fff;
$tabs-border: #bbb;


/*
 * Basic positioning styles shared by primary and secondary tabs.
 */

ul.primary,
ul.secondary {
  @include clearfix;
  border-bottom: 1px solid $tabs-border;
  list-style: none;
  margin: 1em 0 1.5em;
  padding: 0 2px;
  white-space: nowrap;

  li {
    float: left; /* LTR */
    margin: 0.5em 3px 0;
  }

  a {
    border: 1px solid #e9e9e9;
    border-right: 0;
    border-bottom: 0;
    display: block;
    line-height: 1.5em;
    text-decoration: none;
  }
}

/*
 * Primary tabs
 */

ul.primary {
  li {
    @include border-top-radius(4px);
    @include single-text-shadow(#fff, 1px, 1px, 0);
    border: 1px solid $tabs-border;
    border-bottom: 0;
    margin: 0.5em 0 0 3px; /* LTR */
  }
  li.active {
    border-bottom: 1px solid $tabs-container-bg;
    margin-bottom: -1px; /* Overlap the ul's border. */
  }

  a:link,
  a:visited {
    @include border-top-radius(4px);
    @include transition(background-color 0.3s);
    color: #333;
    background-color: #dedede;
    letter-spacing: 1px;
    padding: 0 1em;
    text-align: center;
  }
  a:hover,
  a:focus {
    background-color: #e9e9e9;
    border-color: #f2f2f2;
  }
  a.active {
    background-color: transparent;
    @include filter-gradient(rgba(#e9e9e9, 1), rgba(#e9e9e9, 0));
    @include background-image(linear-gradient(rgba(#e9e9e9, 1), rgba(#e9e9e9, 0)));
    border-color: #fff;
  }
}

/*
 * Secondary tabs
 */

ul.secondary {
  font-size: .9em;
  margin-top: -1.5em; /* Collapse bottom margin of ul.primary. */
  padding-bottom: .5em;

  a:link,
  a:visited {
    @include border-radius(.75em);
    @include transition(background-color 0.3s);
    @include single-text-shadow(#fff, 1px, 1px, 0);
    background-color: #f2f2f2;
    color: #666;
    padding: 0 .5em;
  }
  a:hover,
  a:focus {
    background-color: #dedede;
    border-color: #999;
    color: #333;
  }
  a.active,
  a:active {
    @include single-text-shadow(#333, 1px, 1px, 0);
    background-color: #666;
    border-color: #000;
    color: #fff;
  }
}
:@children[o:Sass::Tree::CommentNode;[ :@options{:importero: Sass::Importers::Filesystem:
@root"W/var/www/igtc/igtc-partners-stage.k4hdev.org/htdocs/sites/all/themes/tobacco7/sass:@silent0:@lines[ :@value"�/**
 * @file
 * Tabs Styling
 *
 * Adds styles for the primary and secondary tabs.
 *
 * Compare this with default CSS found in the system module's stylesheet (a copy
 * of which is in drupal7-reference.css.)
 */:
@linei:
@loud0o:Sass::Tree::ImportNode
;0;[ ;
@
:@imported_filename"	base;io;	;[ ;
@
;i ;[ ;">/* Some variables to make altering the styling easier. */;i;0o:Sass::Tree::VariableNode:
@expro:Sass::Script::Color	:@attrs{	:redi�:
alphai:
greeni�:	bluei�;
{ ;0;i;[ :
@name"tabs-container-bg;
@
:@guarded0;io;;o;	;{	;i�;i;i�;i�;
{ ;0;i;[ ;"tabs-border;
@
;0;io;	;[ ;
@
;0;[ ;"M/*
 * Basic positioning styles shared by primary and secondary tabs.
 */;i;0o:Sass::Tree::RuleNode;T;[o:Sass::Tree::MixinNode;[ ;"clearfix;
@
:@keywords{ :
@args[ ;io:Sass::Tree::PropNode:@importantF;[ :
@tabsi :@prop_syntax:new;["border-bottom;
@
;o:Sass::Script::List	:@separator:
space;
{ ;[o:Sass::Script::Number:@numerator_units["px;
@2:@original"1px:@denominator_units[ ;i;io:Sass::Script::String	:
@type:identifier;
@2;"
solid;io:Sass::Script::Variable	;"tabs-border;
@2:@underscored_name"tabs_border;i;i;io;#;$F;[ ;%i ;&;';["list-style;
@
;o;/;0;1;"	none;i o;#;$F;[ ;%i ;&;';["margin;
@
;o;/;0;1;"1em 0 1.5em;i!o;#;$F;[ ;%i ;&;';["padding;
@
;o;/;0;1;"
0 2px;i"o;#;$F;[ ;%i ;&;';["white-space;
@
;o;/;0;1;"nowrap;i#o;;T;[o;#;$F;[ ;%i ;&;';["
float;
@
;o;/;0;1;"	left;i&o;	;[ ;
@
;0;[ ;"/* LTR */;i&;0o;#;$F;[ ;%i ;&;';["margin;
@
;o;/;0;1;"0.5em 3px 0;i';%i ;
@
:
@rule["li:@parsed_ruleso:"Sass::Selector::CommaSequence:@members[o:Sass::Selector::Sequence;7[o:#Sass::Selector::SimpleSequence;7[o:Sass::Selector::Element	:@namespace0;["li:@filename" ;i%;<@s;i%;<@s;i%;i%o;;T;[o;#;$F;[ ;%i ;&;';["border;
@
;o;/;0;1;"1px solid #e9e9e9;i+o;#;$F;[ ;%i ;&;';["border-right;
@
;o;/;0;1;"0;i,o;#;$F;[ ;%i ;&;';["border-bottom;
@
;o;/;0;1;"0;i-o;#;$F;[ ;%i ;&;';["display;
@
;o;/;0;1;"
block;i.o;#;$F;[ ;%i ;&;';["line-height;
@
;o;/;0;1;"
1.5em;i/o;#;$F;[ ;%i ;&;';["text-decoration;
@
;o;/;0;1;"	none;i0;%i ;
@
;4["a;5o;6;7[o;8;7[o;9;7[o;:	;;0;["a;<" ;i*;<@�;i*;<@�;i*;i*;%i ;
@
;4["ul.primary,
ul.secondary;5o;6;7[o;8;7[o;9;7[o;:	;;0;["ul;<" ;io:Sass::Selector::Class;["primary;<@�;i;<@�;io;8;7["
o;9;7[o;:	;;0;["ul;<@�;io;=;["secondary;<@�;i;<@�;i;<@�;i;io;	;[ ;
@
;0;[ ;"/*
 * Primary tabs
 */;i4;0o;;T;[
o;;T;[o; ;[ ;"border-top-radius;
@
;!{ ;"[o;+;,["px;
{ ;-"4px;.[ ;i	;i:;i:o; ;[ ;"single-text-shadow;
@
;!{ ;"[	o;	;{	;i�;i;i�;i�;
{ ;0;i;o;+;,["px;
@�;-"1px;.[ ;i;i;o;+;,["px;
@�;-"1px;.[ ;i;i;o;+;,[ ;
@�;-"0;.[ ;i ;i;;i;o;#;$F;[ ;%i ;&;';["border;
@
;o;(	;);*;
{ ;[o;+;,["px;
@�;-"1px;.[ ;i;i<o;/	;0;1;
@�;"
solid;i<o;2	;"tabs-border;
@�;3"tabs_border;i<;i<;i<o;#;$F;[ ;%i ;&;';["border-bottom;
@
;o;/;0;1;"0;i=o;#;$F;[ ;%i ;&;';["margin;
@
;o;/;0;1;"0.5em 0 0 3px;i>o;	;[ ;
@
;0;[ ;"/* LTR */;i>;0;%i ;
@
;4["li;5o;6;7[o;8;7[o;9;7[o;:	;;0;["li;<" ;i9;<@;i9;<@;i9;i9o;;T;[o;#;$F;[ ;%i ;&;';["border-bottom;
@
;o;(	;);*;
{ ;[o;+;,["px;
@;-"1px;.[ ;i;iAo;/	;0;1;
@;"
solid;iAo;2	;"tabs-container-bg;
@;3"tabs_container_bg;iA;iA;iAo;#;$F;[ ;%i ;&;';["margin-bottom;
@
;o;+;,["px;
{ ;-"	-1px;.[ ;i�;iB;iBo;	;[ ;
@
;0;[ ;"#/* Overlap the ul's border. */;iB;0;%i ;
@
;4["li.active;5o;6;7[o;8;7[o;9;7[o;:	;;0;["li;<" ;i@o;=;["active;<@=;i@;<@=;i@;<@=;i@;i@o;;T;[o; ;[ ;"border-top-radius;
@
;!{ ;"[o;+;,["px;
{ ;-"4px;.[ ;i	;iG;iGo; ;[ ;"transition;
@
;!{ ;"[o;(	;);*;
{ ;[o;/	;0;1;
@T;"background-color;iHo;+;,["s;
@T;-"	0.3s;.[ ;f0.29999999999999999 33;iH;iH;iHo;#;$F;[ ;%i ;&;';["
color;
@
;o;/;0;1;"	#333;iIo;#;$F;[ ;%i ;&;';["background-color;
@
;o;/;0;1;"#dedede;iJo;#;$F;[ ;%i ;&;';["letter-spacing;
@
;o;/;0;1;"1px;iKo;#;$F;[ ;%i ;&;';["padding;
@
;o;/;0;1;"
0 1em;iLo;#;$F;[ ;%i ;&;';["text-align;
@
;o;/;0;1;"center;iM;%i ;
@
;4["a:link,
  a:visited;5o;6;7[o;8;7[o;9;7[o;:	;;0;["a;<" ;iFo:Sass::Selector::Pseudo
;0:
class;["	link:	@arg0;<@�;iF;<@�;iFo;8;7["
o;9;7[o;:	;;0;["a;<@�;iFo;>
;0;?;["visited;@0;<@�;iF;<@�;iF;<@�;iF;iFo;;T;[o;#;$F;[ ;%i ;&;';["background-color;
@
;o;/;0;1;"#e9e9e9;iQo;#;$F;[ ;%i ;&;';["border-color;
@
;o;/;0;1;"#f2f2f2;iR;%i ;
@
;4["a:hover,
  a:focus;5o;6;7[o;8;7[o;9;7[o;:	;;0;["a;<" ;iPo;>
;0;?;["
hover;@0;<@�;iP;<@�;iPo;8;7["
o;9;7[o;:	;;0;["a;<@�;iPo;>
;0;?;["
focus;@0;<@�;iP;<@�;iP;<@�;iP;iPo;;T;[	o;#;$F;[ ;%i ;&;';["background-color;
@
;o;/;0;1;"transparent;iUo; ;[ ;"filter-gradient;
@
;!{ ;"[o:Sass::Script::Funcall
;"	rgba;
{ ;!{ ;"[o;	;{	;i�;i;i�;i�;
@�;0;iVo;+;,[ ;
@�;-"1;.@�;i;iV;iVo;A
;"	rgba;
@�;!{ ;"[o;	;{	;i�;i;i�;i�;
@�;0;iVo;+;,[ ;
@�;-"0;.@�;i ;iV;iV;iVo; ;[ ;"background-image;
@
;!{ ;"[o;A
;"linear-gradient;
{ ;!{ ;"[o;A
;"	rgba;
@�;!{ ;"[o;	;{	;i�;i;i�;i�;
@�;0;iWo;+;,[ ;
@�;-"1;.@�;i;iW;iWo;A
;"	rgba;
@�;!{ ;"[o;	;{	;i�;i;i�;i�;
@�;0;iWo;+;,[ ;
@�;-"0;.@�;i ;iW;iW;iW;iWo;#;$F;[ ;%i ;&;';["border-color;
@
;o;/;0;1;"	#fff;iX;%i ;
@
;4["a.active;5o;6;7[o;8;7[o;9;7[o;:	;;0;["a;<" ;iTo;=;["active;<@;iT;<@;iT;<@;iT;iT;%i ;
@
;4["ul.primary;5o;6;7[o;8;7[o;9;7[o;:	;;0;["ul;<" ;i8o;=;["primary;<@;i8;<@;i8;<@;i8;i8o;	;[ ;
@
;0;[ ;"/*
 * Secondary tabs
 */;i\;0o;;T;[o;#;$F;[ ;%i ;&;';["font-size;
@
;o;/;0;1;"	.9em;iao;#;$F;[ ;%i ;&;';["margin-top;
@
;o;+;,["em;
{ ;-"-1.5em;.[ ;f	-1.5;ib;ibo;	;[ ;
@
;0;[ ;"0/* Collapse bottom margin of ul.primary. */;ib;0o;#;$F;[ ;%i ;&;';["padding-bottom;
@
;o;/;0;1;"	.5em;ico;;T;[o; ;[ ;"border-radius;
@
;!{ ;"[o;+;,["em;
{ ;-"0.75em;.[ ;f	0.75;ig;igo; ;[ ;"transition;
@
;!{ ;"[o;(	;);*;
{ ;[o;/	;0;1;
@S;"background-color;iho;+;,["s;
@S;-"	0.3s;.[ ;f0.29999999999999999 33;ih;ih;iho; ;[ ;"single-text-shadow;
@
;!{ ;"[	o;	;{	;i�;i;i�;i�;
{ ;0;iio;+;,["px;
@d;-"1px;.[ ;i;iio;+;,["px;
@d;-"1px;.[ ;i;iio;+;,[ ;
@d;-"0;.@�;i ;ii;iio;#;$F;[ ;%i ;&;';["background-color;
@
;o;/;0;1;"#f2f2f2;ijo;#;$F;[ ;%i ;&;';["
color;
@
;o;/;0;1;"	#666;iko;#;$F;[ ;%i ;&;';["padding;
@
;o;/;0;1;"0 .5em;il;%i ;
@
;4["a:link,
  a:visited;5o;6;7[o;8;7[o;9;7[o;:	;;0;["a;<" ;ifo;>
;0;?;["	link;@0;<@�;if;<@�;ifo;8;7["
o;9;7[o;:	;;0;["a;<@�;ifo;>
;0;?;["visited;@0;<@�;if;<@�;if;<@�;if;ifo;;T;[o;#;$F;[ ;%i ;&;';["background-color;
@
;o;/;0;1;"#dedede;ipo;#;$F;[ ;%i ;&;';["border-color;
@
;o;/;0;1;"	#999;iqo;#;$F;[ ;%i ;&;';["
color;
@
;o;/;0;1;"	#333;ir;%i ;
@
;4["a:hover,
  a:focus;5o;6;7[o;8;7[o;9;7[o;:	;;0;["a;<" ;ioo;>
;0;?;["
hover;@0;<@�;io;<@�;ioo;8;7["
o;9;7[o;:	;;0;["a;<@�;ioo;>
;0;?;["
focus;@0;<@�;io;<@�;io;<@�;io;ioo;;T;[	o; ;[ ;"single-text-shadow;
@
;!{ ;"[	o;	;{	;i8;i;i8;i8;
{ ;0;ivo;+;,["px;
@�;-"1px;.[ ;i;ivo;+;,["px;
@�;-"1px;.[ ;i;ivo;+;,[ ;
@�;-"0;.@�;i ;iv;ivo;#;$F;[ ;%i ;&;';["background-color;
@
;o;/;0;1;"	#666;iwo;#;$F;[ ;%i ;&;';["border-color;
@
;o;/;0;1;"	#000;ixo;#;$F;[ ;%i ;&;';["
color;
@
;o;/;0;1;"	#fff;iy;%i ;
@
;4["a.active,
  a:active;5o;6;7[o;8;7[o;9;7[o;:	;;0;["a;<" ;iuo;=;["active;<@ ;iu;<@ ;iuo;8;7["
o;9;7[o;:	;;0;["a;<@ ;iuo;>
;0;?;["active;@0;<@ ;iu;<@ ;iu;<@ ;iu;iu;%i ;
@
;4["ul.secondary;5o;6;7[o;8;7[o;9;7[o;:	;;0;["ul;<" ;i`o;=;["secondary;<@;i`;<@;i`;<@;i`;i`;
@
;i