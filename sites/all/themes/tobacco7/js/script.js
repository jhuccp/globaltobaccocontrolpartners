/**
 * @file
 * A JavaScript file for the theme.
 *
 * In order for this JavaScript to be loaded on pages, see the instructions in
 * the README.txt next to this file.
 */

// JavaScript should be made compatible with libraries other than jQuery by
// wrapping it with an "anonymous closure". See:
// - http://drupal.org/node/1446420
// - http://www.adequatelygood.com/2010/3/JavaScript-Module-Pattern-In-Depth
(function ($, Drupal, window, document, undefined) {

// Place your code here.
window.onload=function() {
   headerHeight();
}
window.onresize=function() { 
   headerHeight();
}
function headerHeight() {
   if ($('#header').width()>600) {
    $('.region-header').height($('.region-header img').height()); 
    /*$('div.region-header').show();
    $('h1#site-name').css("text-indent","-9999px");
    $('h1#site-name').css("height","0");
    $('#site-name').hide();*/
  } else {
    /*$('div.region-header').hide();
    $('h1#site-name').css("text-indent","0");
    $('h1#site-name').css("height","auto");
    $('#site-name').show();*/
  } 
}

$(document).ready(function() {
  $('.rollover').hover(function() {
    var src = $(this).attr('src');
    var src_filename = src.substr(0, src.lastIndexOf('.')) || src;
    var src_ext = src.substr(src.lastIndexOf('.'), src.length) || '';
    $(this).attr('src', src_filename + "_rollover" + src_ext);
  }, function() {
    var src = $(this).attr('src');
    var src_filename = src.substr(0, src.lastIndexOf('_rollover')) || src;
    var src_ext = src.substr(src.lastIndexOf('.'), src.length) || '';
    $(this).attr('src', src_filename + src_ext);
  });
});


})(jQuery, Drupal, this, this.document);
