<?php

/**
 * @file
 * Functions and forms for the frontend part of newsletter module.
 */

/**
 * Access callback for user newsletter subscriptions editing.
 */
function newsletter_edit_subsdcriber_access($account) {
  return (($GLOBALS['user']->uid == $account->uid && user_access('subscribe newsletters')) || user_access('administer users')) && $account->uid > 0;
}

/**
 * Loads up the form that is displayed to the newsletter block.
 */
function newsletter_subscribe_form($form, &$form_state) {
  global $user;
  $ajax = array(
    'callback' => 'newsletter_subscribe_ajax',
    'wrapper' => 'error',
    'method' => 'append',
    'effect' => 'fade',
    'progress' => array(
      'type' => 'throbber',
      'message' => NULL,
    ),
    'event' => 'click',
  );
  if (!isset($user->mail) || variable_get('newsletter_show_email_in_block', FALSE)) {
    $form['email'] = array(
      '#type' => 'textfield',
      '#default_value' => 'user@example.com',
      '#size' => 17,
      '#required' => TRUE,
      '#prefix' => '<div id="error"></div>',
    );
  }
  else {
    $form['logged-in'] = array(
      '#type' => 'hidden',
      '#value' => TRUE,
    );
    $form['logged-in']['msg'] = array(
      '#type' => 'markup',
      '#markup' => t('Hello @user.Please consider joining our newsletter lists!', array('@user' => $user->name)),
    );
  }

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Subscribe'),
    '#prefix' => '<div id="subscribe" class="button green">',
    '#suffix' => '</div>',
    '#ajax' => $ajax,
  );
  return $form;
}

/**
 * Validates e-mail and sends user to next step which is
 * a colorbox if the module exists.Else simple http redirect.
 */
function newsletter_subscribe_ajax($form, &$form_state) {
  if (isset($form_state['values']['logged-in'])) {
    global $user;
    $email = $user->mail;
  }
  elseif (!isset($form_state['values']['logged-in'])) {
    $email = ($form_state['input']['email']!='user@example.com')
    ? $form_state['input']['email']
    : '';
  }

  if (valid_email_address($email) && !newsletter_is_subscribed($email)) {
    $q = array(
      'destination' => '',
      'email' => $email,
    );
    $colorbox_url = url('colorbox/form/newsletter_manage_subscriptions_form', array('query' => $q));
    $url = url('newsletter/subscribe', array('query' => $q));
    return module_exists('colorbox') ?
      "<script type='text/javascript'>
        (function ($) {
          $.fn.colorbox({
            href: '$colorbox_url',
            width: 600,
            height: 650,
            open: true,
            onComplete: function(){Drupal.behaviors.newsletter.subscribeForm();},
          })
        })(jQuery);
      </script>" :
      "<script type='text/javascript'>
        location.href='$url'
      </script>";
  }
  else {
    return t("This e-mail doesn't exist or you have already subscribed");
  }
}

/**
 * The second form step of a newsletter subscription.
 * @TODO get terms of exposed lists.
 */
function newsletter_subscribe_advanced($form, &$form_state) {

  return drupal_get_form('newsletter_manage_subscriptions_form', $account, $subscriber);
}

/*
 * Menu callback; Lets users manage their subscriptions.
 */
function newsletter_manage_subscriptions_form($form, &$form_state, $user = NULL) {
  if (!isset($user)) {
    global $user;
    $subscriber = entity_get_controller('newsletter_subscriber')->create();
    $subscriber->email = isset($_GET['email']) ? check_plain($_GET['email']) : @$user->mail;

  }
  elseif (arg(0) == 'newsletter' && arg(1) == 'edit') {
    unset($user);
    global $user;
    $subscribers = newsletter_subscriber_load(array(), array('hash' => arg(2)));
    $subscriber = !empty($subscribers) ? array_pop($subscribers) : entity_get_controller('newsletter_subscriber')->create();
  }
  else {
    $subscribers = newsletter_subscriber_load(array(), array('uid' => $user->uid));
    $subscriber = !empty($subscribers) ? array_pop($subscribers) : entity_get_controller('newsletter_subscriber')->create();
  }
  newsletter_add_js();

  $show_email = variable_get('newsletter_show_email', TRUE);

  $form['uid'] = array(
    '#type' => 'hidden',
    '#value' => isset($user->uid) ? $user->uid : NULL,
  );
  $form['nsid'] = array(
    '#type' => 'hidden',
    '#value' => isset($subscriber->nsid) ? $subscriber->nsid : NULL,
  );
  if ($show_email) {
    $form['mail'] = array(
      '#attributes' => array('class' => array('container-inline')),
      '#type' => 'fieldset',
      '#title' => t('E-mail'),
      '#weight' => -10,
    );
  }
  $form['mail']['email'] = array(
    '#type' => $show_email ? 'textfield' : 'hidden',
    '#default_value' => !empty($subscriber->email) ? $subscriber->email : @$user->email,
    '#required' => TRUE,
    '#size' => 20,
  );
  $personal_info_form = variable_get('newsletter_personal_info_form');
  if ( in_array('show', $personal_info_form) || in_array('require', $personal_info_form) ) {
    $form['info'] = array(
      '#type' => 'fieldset',
      '#title' => t('Personal Info'),
      '#weight' => 5,
    );
  }

  foreach ($personal_info_form as $info_type => $value) {
    if ($value != 'hidden') {
      switch ($info_type) {
        case 'firstname':
          $form['info']['firstname'] = array(
            '#type' => 'textfield',
            '#title' => t('First Name'),
            '#size' => 40,
            '#required' => $value == 'require' ? TRUE : FALSE,
            '#default_value' => $subscriber->firstname,
          );
          break;
        case 'lastname':
          $form['info']['lastname'] = array(
            '#type' => 'textfield',
            '#title' => t('Last Name'),
            '#size' => 40,
            '#required' => $value == 'require' ? TRUE : FALSE,
            '#default_value' => $subscriber->lastname,
          );
          break;
        case 'gender':
          $form['info']['gender'] = array(
            '#type' => 'select',
            '#title' => t('Gender'),
            '#options' => array(
              'male' => t('Male'),
              'female' => t('Female'),
              'other' => t('Other'),
            ),
            '#empty_option' =>  t('- Select -'),
            '#required' => $value == 'require' ? TRUE : FALSE,
            '#default_value' => $subscriber->gender,
          );
          break;
        case 'receive_format':
          $form['info']['receive_format'] = array(
            '#type' => 'select',
            '#title' => t('Preferred Format'),
            '#options' => array(
              'html' => t('HTML'),
              'plain' => t('Plain Text'),
            ),
            '#empty_option' =>  t('- Select -'),
            '#required' => $value == 'require' ? TRUE : FALSE,
            '#default_value' => $subscriber->receive_format,
          );
          break;
      }
    }
  }
  field_attach_form('newsletter_subscriber', $subscriber, $form, $form_state);
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => isset($subscriber->nsid) ?  t('Update subscription') : t('Subscribe'),
    '#weight' => 10,
  );

  return $form;
}

function newsletter_manage_subscriptions_form_submit($form, &$form_state) {
  $needs_confirm = variable_get('newsletter_send_confirm');
  $subscriber = (object) $form_state['input'];
  if (empty($form_state['values']['nsid'])) {
    unset($subscriber->nsid);
  }
  else {
    $needs_confirm = FALSE;
  }
  if (empty($form_state['values']['uid'])) {
    $subscriber->uid = NULL;
  }

  field_attach_submit('newsletter_subscriber', $subscriber, $form, $form_state);
  entity_get_controller('newsletter_subscriber')->save($subscriber, $needs_confirm);
}

/**
 * Confirms a subscription to a newsletter list.
 */
function newsletter_confirm() {
  $hash = $_GET['hash'];
  $email = db_query('SELECT email
    FROM {newsletter_subscriber}
    WHERE hash = :hash',
    array(':hash' => $hash))
    ->fetchField();
  if ($email) {
    db_update('newsletter_subscriber')
    ->fields(array(
      'confirmed' => 1,
      'confirmation_timestamp' => REQUEST_TIME,
    ))
    ->condition('hash', $hash)
    ->execute();

    if (variable_get('newsletter_send_welcome', FALSE)) {
      $welcome_email = newsletter_create()->sendBasic(2, $email);
    }
  }
  return $email
    ? t('You have successfully confirmed your subscription!')
    : t('The hash you provided is invalid or outdated');
}

/**
 * Unsubscribes a subscriber from newsletter.
 */
function newsletter_unsubscribe() {
  $hash = $_GET['hash'];
  $email = db_query('SELECT email
    FROM {newsletter_subscriber}
    WHERE hash = :hash',
    array(':hash' => $hash))
    ->fetchField();
  if ($email) {

    if (variable_get('newsletter_send_unsubscribe', FALSE)) {
      $unsubscribe_email = newsletter_create()->sendBasic(3, $email);
    }

    db_delete('newsletter_subscriber')
      ->condition('hash', $hash)
      ->execute();
  }
  return $email
    ? t('You have successfully unsubscribed from our newsletter')
    : t('The hash you provided is invalid or outdated');
}

/*
 * Updates the opened newsletters.
 */
function newsletter_statistics_open() {
  $subscriber = db_query('SELECT *
    FROM {newsletter_subscriber}
    WHERE hash = :hash',
    array(':hash' => $_GET['hash']))
    ->fetchField();

  if ($subscriber) {
    db_query('UPDATE {newsletter_newsletter}
      SET opens = opens + 1
      WHERE nnid = :id',
      array(':id' => $_GET['source']));
  }
}
