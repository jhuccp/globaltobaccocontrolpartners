<?php

/**
 * @file
 * Several helper vital functions for newsletter module.
 */


/**
 * Loads up the requested list.
 */
function newsletter_list_load($nlids = FALSE, $conditions = array(), $reset = FALSE) {
  if (is_numeric($nlids)) {
    $id = $nlids;
    $nlids = !empty($nlids) ? array($nlids) : array();
  }
  $lists = entity_load('newsletter_list', $nlids, $conditions, $reset);
  return isset($id) ? $lists[$id] : $lists;
}

/**
 * Loads up the requested subscriber.
 */
function newsletter_subscriber_load($nsids = FALSE, $conditions = array(), $reset = FALSE) {
  if (is_numeric($nsids)) {
    $id = $nsids;
    $nsids = !empty($nsids) ? array($nsids) : array();
  }
  $subscribers = entity_load('newsletter_subscriber', $nsids, $conditions, $reset);
  return isset($id) ? $subscribers[$id] : $subscribers;
}

/**
 * Loads up the requested template.
 */
function newsletter_template_load($ntids = FALSE, $conditions = array(), $reset = FALSE) {
  if (is_numeric($ntids)) {
    $id = $ntids;
    $ntids = !empty($ntids) ? array($ntids) : array();
  }
  $templates = entity_load('newsletter_template', $ntids, $conditions, $reset);
  return isset($id) ? $templates[$id] : $templates;
}

/**
 * Loads up the requested newsletter.
 */
function newsletter_newsletter_load($nnids = FALSE, $conditions = array(), $reset = FALSE) {
  if (is_numeric($nnids)) {
    $id = $nnids;
    $nnids = !empty($nnids) ? array($nnids) : array();
  }
  $newsletters = entity_load('newsletter_newsletter', $nnids, $conditions, $reset);
  return isset($id) ? $newsletters[$id] : $newsletters;
}

/**
 * Get the subscribers of a newsletter list.
 *
 * @param $nlid
 *   The id of the newsletter list.
 *
 * @return
 *   Array containg the subscriber full objects.Else empty array.
 */
function newsletter_subscribers_by_list($nlid, $filter) {
  $subscribers = array();
  if ($filter) {
    $confirm = variable_get('newsletter_send_confirm', TRUE);
    $all_subscribers = $confirm ? newsletter_subscriber_load(FALSE, array('confirmed' => 1)) : newsletter_subscriber_load();
  }
  else {
    $all_subscribers = newsletter_subscriber_load();
  }
  foreach ($all_subscribers as $subscriber) {
    $lists = field_get_items('newsletter_subscriber', $subscriber, 'field_newsletter_list');
    if ($lists) {
      foreach ($lists as $list) {
        if ($list['target_id'] == $nlid) {
          $subscribers[] = $subscriber;
        }
      }
    }
  }
  return $subscribers;
}

/**
 * Checks whether an email is registered to newsletter list or not.
 * @return
 *  boolean TRUE or FALSE.
 */
function newsletter_is_subscribed($email = NULL) {
  if (!isset($email)) {
    global $user;
    $email = isset($user->mail) ? $user->mail : FALSE;
  }
  if ($email) {
    $is_subscribed = db_query('SELECT email FROM {newsletter_subscriber} WHERE email = :email', array(':email' => $email))->fetchField();
    return $is_subscribed ? TRUE : FALSE;
  }
  else {
    return FALSE;
  }
}

/**
 * Instantiates a new Newsletter class according to http://drupal.org/node/608152.
 *
 * @return
 *   A newsletter object.
 */
function newsletter_create($nlid=NULL, $ntid=NULL, $nnid=NULL) {
  if (isset($ntid)) {
    $newsletter = new NewsletterMailCustom($nlid, $ntid, $nnid);
  }
  elseif (isset($nlid) && !isset($ntid)) {
    $newsletter = new NewsletterMail($nlid);
  }
  else {
    $newsletter = new NewsletterBasic();
  }
  return (object) $newsletter;
}

/*
 * Returns an image to be added to newsletter body
 * so we can calculate the open rate.
 *
 * @param $hash
 *   Subscriber's personal hash.
 * @param $newsletter
 *   The newsletter object.
 *
 * @return
 *   An html image string.
 */
function newsletter_add_open_rate_image($hash, $newsletter) {
  $params = array(
    'absolute' => TRUE,
    'query' => array(
      'hash' => $hash,
      'source' => $newsletter->nnid,
    ),
  );
  $url = url('newsletter/statistics', $params);
  $image = "<img src='$url' width = '1' height = '1' />";
  return $image;
}

/**
 * Sets a watchdog message for the just sent newsletter.
 *
 * @param $id
 *   The newsletter id.
 * @param $title
 *   The subscriber's list title.
 * @param $email
 *   The subscriber's email.
 * @param $result
 *   The result from drupal_mail_system.
 */
function newsletter_set_watchdog($id, $title, $email, $result) {
  watchdog('newsletter', 'Newsletter list with newsletter id @news_id and list name @list_name sent to @sub with status code @status.',
    array(
      '@news_id' => $id,
      '@list_name' => $title,
      '@sub' => $email,
      '@status' => $result,
    ), WATCHDOG_ERROR);
}

/**
 * @return
 *   An array of newsletter lists with exposed templates.
 */
function newsletter_exposed_lists() {
  $exposed_lists = array();
  $lists = newsletter_list_load();
  foreach ($lists as $list) {
    $ntids = field_get_items('newsletter_list', $list, 'field_newsletter_template');
    foreach ($ntids as $ntid) {
      $template = newsletter_template_load($ntid['target_id']);
      if ($template->exposed) {
        $exposed_lists[] = $list->nlid;
      }
    }
  }
  return $exposed_lists;
}

/**
 * Fetches the terms for a template.
 *
 * @param
 *   $ntid The template ntid.
 *
 * @return
 *   An array of term names keyed by their tids.
 */
function newsletter_get_template_terms($ntid) {
  $terms = array();
  $template = newsletter_template_load($ntid);
  $vocabularies = taxonomy_get_vocabularies();
  foreach ($vocabularies as $vocabulary) {
    if ($vocabulary->machine_name == 'newsletter_categories') {
      continue;
    }
    $vocabulary_field = field_get_items('newsletter_template', $template, 'field_' . $vocabulary->machine_name);
    if ($vocabulary_field) {
      foreach ($vocabulary_field as $values) {
        $term = taxonomy_term_load($values['tid']);
        if ($term) {
          $terms[$term->tid] = $term->name;
        }
      }
    }
  }
  return $terms;
}

/**
 * Get a subscriber's exposed terms.
 */
function newsletter_get_default_exposed($subscriber, $list) {
  if (!isset($subscriber->nsid)) {
    return array();
  }
  $tids = db_query('SELECT target_id_tids
    FROM {field_data_field_newsletter_list}
    WHERE entity_id = :nsid
    AND field_newsletter_list_target_id = :nlid',
    array(':nsid' => $subscriber->nsid, ':nlid' => $list->nlid))->fetchField();
  if ($tids) {
    $tids = @unserialize($tids);
    if (is_array($tids)) {
      foreach ($tids as $tid) {
        $defaults[$tid] = $tid;
      }
    }
  }

  return isset($defaults) ? $defaults : array();
}

/**
 * @return
 *   An array of newsletter lists that are set to send with custom schedule.
 */
function newsletter_custom_send_rate_lists() {
  $lists = db_query('SELECT nlid, send_rate FROM {newsletter_list}')->fetchAll();
  foreach ($lists as $list) {
    if (is_numeric($list->send_rate)) {
      $custom_lists[]=$list;
    }
  }
  return isset($custom_lists) ? $custom_lists : array();
}

/**
 * Feedback to site administrator depending on newsletter send status.
 *
 * @param $statuses
 *   An array containing status values set when a newsletter mail was sent.
 */
function newsletter_feedback($statuses) {
  if (is_array($statuses) && !empty($statuses)) {

    $success = 0;
    $error = 0;
    $no_items = 0;
    $no_subs = 0;

    foreach ($statuses as $status) {
      $status = ($status === 1 || $status === TRUE) ? 'success' : $status;
      switch ($status) {
        case 'No subscribers':
          $no_subs++;
          break;
        case 'No items':
          $no_items++;
          break;
        case 'success':
          $success++;
          break;
        default:
          $error++;
      }
    }

    if ($no_subs) {
      watchdog('newsletter', '%no_subs Newsletter Lists failed to send because no subscribers found subscribed to them',
        array('%no_subs' => $no_subs));
    }
    if ($no_items) {
      watchdog('newsletter', '%no_items Newsletter Lists failed to send because no published nodes found for their terms',
        array('%no_items' => $no_items));
    }
    if ($success) {
      watchdog('newsletter', '%success Newsletter Lists sent',
        array('%success' => $success));
    }
    if ($error) {
      watchdog('newsletter', '%error Newsletter Lists failed to send.Check previous watchdog notices for more info',
        array('%error' => $error), WATCHDOG_ERROR);
    }

  }
  return;
}
/**
 * Add newsletter js file and settings.
 */
function newsletter_add_js() {
  static $added;
  if ($added) {
    return;
  }
  drupal_add_js(drupal_get_path('module', 'newsletter') . '/js/newsletter.js');
  $lists = newsletter_exposed_lists();

  if (!field_is_translatable('newsletter_subscriber', 'field_newsletter_list')) {
    $lang = LANGUAGE_NONE;
  }
  else {
    $lang = language_default($language);
  }
  drupal_add_js(array( 'exposed' => $lists, 'lang' => $lang), 'setting');
  if (!module_exists('colorbox')) {
    drupal_add_js('(function ($) {$(document).ready(function () {
      Drupal.behaviors.newsletter.subscribeForm();
    });})(jQuery);', 'inline');
  }
  $added++;
}
