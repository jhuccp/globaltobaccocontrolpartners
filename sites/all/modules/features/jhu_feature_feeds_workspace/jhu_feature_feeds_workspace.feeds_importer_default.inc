<?php
/**
 * @file
 * jhu_feature_feeds_workspace.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_workspace_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'workspace';
  $feeds_importer->config = array(
    'name' => 'Workspace',
    'description' => 'Import Workspace nodes',
    'fetcher' => array(
      'plugin_key' => 'MNSapiFetcher',
      'config' => array(
        'list_url' => 'mn_sapi/list/node/workspace',
        'get_url' => 'mn_sapi/get/node/[id]',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MNSapiParser',
      'config' => array(
        'serialized_example' => 'O:8:"stdClass":26:{s:3:"nid";s:1:"9";s:3:"vid";s:1:"9";s:4:"type";s:9:"workspace";s:6:"status";s:1:"1";s:7:"created";s:10:"1201622334";s:7:"changed";s:10:"1203023985";s:7:"comment";s:1:"2";s:7:"promote";s:1:"0";s:6:"sticky";s:1:"0";s:18:"revision_timestamp";s:10:"1203023985";s:5:"title";s:18:"China Country Team";s:4:"body";s:0:"";s:6:"teaser";s:0:"";s:3:"log";s:0:"";s:6:"format";s:1:"0";s:3:"uid";s:1:"1";s:4:"name";s:12:"tobaccoadmin";s:7:"picture";s:28:"files/pictures/picture-1.png";s:4:"data";s:432:"a:16:{s:18:"admin_compact_mode";b:0;s:8:"options1";s:1:"3";s:5:"pass1";s:0:"";s:8:"oldemail";s:0:"";s:8:"newemail";s:16:"gchalk@jhsph.edu";s:8:"options2";s:1:"3";s:5:"pass2";s:0:"";s:7:"contact";i:1;s:8:"options4";s:1:"0";s:5:"pass4";s:0:"";s:14:"picture_delete";i:0;s:14:"picture_upload";s:0:"";s:19:"mass_contact_optout";i:0;s:16:"privatemsg_allow";i:1;s:28:"privatemsg_setmessage_notify";i:1;s:20:"privatemsg_mailalert";s:1:"0";}";s:13:"print_display";i:1;s:21:"print_display_comment";i:0;s:21:"print_display_urllist";i:1;s:22:"last_comment_timestamp";s:10:"1201622334";s:17:"last_comment_name";N;s:13:"comment_count";s:1:"0";s:8:"taxonomy";a:2:{i:102;O:8:"stdClass":5:{s:3:"tid";s:3:"102";s:3:"vid";s:2:"10";s:4:"name";s:18:"China Country Team";s:11:"description";s:73:"This forum is for discussion of issues related to the China Country Team.";s:6:"weight";s:1:"0";}i:76;O:8:"stdClass":5:{s:3:"tid";s:2:"76";s:3:"vid";s:1:"5";s:4:"name";s:5:"China";s:11:"description";s:43:"One of the 15 Bloomberg priority countries.";s:6:"weight";s:1:"0";}}}',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'MNSapiNodeProcessor',
      'config' => array(
        'content_type' => 'workspace',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'vid',
            'target' => 'vid',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'created',
            'target' => 'created',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'uid',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'taxonomy',
            'target' => 'field_workspace_forums',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'taxonomy',
            'target' => 'field_workspace_country',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['workspace'] = $feeds_importer;

  return $export;
}
