<?php
/**
 * @file
 * jhu_feature_mass_contact.features.inc
 */

/**
 * Implements hook_node_info().
 */
function jhu_feature_mass_contact_node_info() {
  $items = array(
    'mass_contact' => array(
      'name' => t('Mass Contact Message'),
      'base' => 'node_content',
      'description' => t('Archived copy of mass e-mails sent from this site'),
      'has_title' => '1',
      'title_label' => t('Subject'),
      'help' => '',
    ),
  );
  return $items;
}
