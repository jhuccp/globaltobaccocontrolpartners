<?php
/**
 * @file
 * jhu_feature_feeds_taxonomy_terms.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_taxonomy_terms_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'taxonomy_term';
  $feeds_importer->config = array(
    'name' => 'Taxonomy term',
    'description' => 'Import taxonomy terms',
    'fetcher' => array(
      'plugin_key' => 'JHUTaxonomyTermFetcher',
      'config' => array(),
    ),
    'parser' => array(
      'plugin_key' => 'JHUTaxonomyTermParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'JHUTaxonomyTermProcessor',
      'config' => array(
        'vocabulary' => 'country',
        'mappings' => array(
          0 => array(
            'source' => 'tid',
            'target' => 'tid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'vid',
            'target' => 'vid',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'name',
            'target' => 'name',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'description',
            'target' => 'description',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '2',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['taxonomy_term'] = $feeds_importer;

  return $export;
}
