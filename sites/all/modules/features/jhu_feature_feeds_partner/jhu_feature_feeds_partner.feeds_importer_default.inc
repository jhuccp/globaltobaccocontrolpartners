<?php
/**
 * @file
 * jhu_feature_feeds_partner.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_partner_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'partner';
  $feeds_importer->config = array(
    'name' => 'Partner Organization',
    'description' => 'Import Partner Organization nodes',
    'fetcher' => array(
      'plugin_key' => 'MNSapiFetcher',
      'config' => array(
        'list_url' => 'mn_sapi/list/node/partner',
        'get_url' => 'mn_sapi/get/node/[id]',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MNSapiParser',
      'config' => array(
        'serialized_example' => 'O:8:"stdClass":27:{s:3:"nid";s:2:"24";s:3:"vid";s:2:"24";s:4:"type";s:7:"partner";s:6:"status";s:1:"1";s:7:"created";s:10:"1201622738";s:7:"changed";s:10:"1207322352";s:7:"comment";s:1:"2";s:7:"promote";s:1:"0";s:6:"sticky";s:1:"0";s:18:"revision_timestamp";s:10:"1207322352";s:5:"title";s:14:"CDC Foundation";s:4:"body";s:1:"1";s:6:"teaser";s:1:"a";s:3:"log";s:0:"";s:6:"format";s:1:"2";s:3:"uid";s:1:"1";s:4:"name";s:12:"tobaccoadmin";s:7:"picture";s:28:"files/pictures/picture-1.png";s:4:"data";s:432:"a:16:{s:18:"admin_compact_mode";b:0;s:8:"options1";s:1:"3";s:5:"pass1";s:0:"";s:8:"oldemail";s:0:"";s:8:"newemail";s:16:"gchalk@jhsph.edu";s:8:"options2";s:1:"3";s:5:"pass2";s:0:"";s:7:"contact";i:1;s:8:"options4";s:1:"0";s:5:"pass4";s:0:"";s:14:"picture_delete";i:0;s:14:"picture_upload";s:0:"";s:19:"mass_contact_optout";i:0;s:16:"privatemsg_allow";i:1;s:28:"privatemsg_setmessage_notify";i:1;s:20:"privatemsg_mailalert";s:1:"0";}";s:13:"print_display";i:1;s:21:"print_display_comment";i:0;s:21:"print_display_urllist";i:1;s:22:"last_comment_timestamp";s:10:"1201622738";s:17:"last_comment_name";N;s:13:"comment_count";s:1:"0";s:8:"taxonomy";a:0:{}s:26:"field_partner_organization";a:1:{i:0;a:1:{s:5:"value";s:1:"2";}}}',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'MNSapiNodeProcessor',
      'config' => array(
        'content_type' => 'partner',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'vid',
            'target' => 'vid',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'created',
            'target' => 'created',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'uid',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'field_partner_organization',
            'target' => 'field_partner_organization',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['partner'] = $feeds_importer;

  return $export;
}
