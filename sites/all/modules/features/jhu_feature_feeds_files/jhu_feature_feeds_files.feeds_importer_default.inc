<?php
/**
 * @file
 * jhu_feature_feeds_files.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_files_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'files';
  $feeds_importer->config = array(
    'name' => 'Files',
    'description' => 'Import files',
    'fetcher' => array(
      'plugin_key' => 'MNSapiFetcher',
      'config' => array(
        'list_url' => 'mn_sapi/list/file',
        'get_url' => 'mn_sapi/get/file/[id]',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MNSapiParser',
      'config' => array(
        'serialized_example' => 'a:6:{s:3:"fid";s:2:"84";s:3:"nid";s:3:"250";s:8:"filename";s:35:"Mexico DF 2004 Ley No fumadores.pdf";s:8:"filepath";s:41:"files/Mexico DF 2004 Ley No fumadores.pdf";s:8:"filemime";s:15:"application/pdf";s:8:"filesize";s:6:"183613";}',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsFileProcessor',
      'config' => array(
        'expire' => -1,
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'fid',
            'target' => 'fid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'filename',
            'target' => 'filename',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'filemime',
            'target' => 'filemime',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'filesize',
            'target' => 'filesize',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'filepath',
            'target' => 'uri',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => 0,
        'input_format' => NULL,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['files'] = $feeds_importer;

  return $export;
}
