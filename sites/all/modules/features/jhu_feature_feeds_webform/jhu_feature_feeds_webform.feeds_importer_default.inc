<?php
/**
 * @file
 * jhu_feature_feeds_webform.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_webform_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'webform';
  $feeds_importer->config = array(
    'name' => 'Webform',
    'description' => 'Import Webform nodes',
    'fetcher' => array(
      'plugin_key' => 'MNSapiFetcher',
      'config' => array(
        'list_url' => 'mn_sapi/list/node/webform',
        'get_url' => 'mn_sapi/get/node/[id]',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MNSapiParser',
      'config' => array(
        'serialized_example' => 'O:8:"stdClass":39:{s:3:"nid";s:3:"158";s:3:"vid";s:3:"158";s:4:"type";s:7:"webform";s:6:"status";s:1:"1";s:7:"created";s:10:"1204035995";s:7:"changed";s:10:"1204036353";s:7:"comment";s:1:"0";s:7:"promote";s:1:"1";s:6:"sticky";s:1:"0";s:18:"revision_timestamp";s:10:"1204036353";s:5:"title";s:14:"Add a New Team";s:4:"body";s:73:"Complete this form and the administrator will help you create a new team.";s:6:"teaser";s:73:"Complete this form and the administrator will help you create a new team.";s:3:"log";s:0:"";s:6:"format";s:1:"1";s:3:"uid";s:1:"1";s:4:"name";s:12:"tobaccoadmin";s:7:"picture";s:28:"files/pictures/picture-1.png";s:4:"data";s:432:"a:16:{s:18:"admin_compact_mode";b:0;s:8:"options1";s:1:"3";s:5:"pass1";s:0:"";s:8:"oldemail";s:0:"";s:8:"newemail";s:16:"gchalk@jhsph.edu";s:8:"options2";s:1:"3";s:5:"pass2";s:0:"";s:7:"contact";i:1;s:8:"options4";s:1:"0";s:5:"pass4";s:0:"";s:14:"picture_delete";i:0;s:14:"picture_upload";s:0:"";s:19:"mass_contact_optout";i:0;s:16:"privatemsg_allow";i:1;s:28:"privatemsg_setmessage_notify";i:1;s:20:"privatemsg_mailalert";s:1:"0";}";s:12:"confirmation";s:48:"Your message has been sent to the administrator.";s:13:"redirect_post";s:1:"0";s:12:"submit_limit";s:2:"-1";s:15:"submit_interval";s:9:"157784630";s:5:"email";s:0:"";s:15:"email_from_name";s:7:"default";s:18:"email_from_address";s:7:"default";s:13:"email_subject";s:7:"default";s:19:"additional_validate";s:0:"";s:17:"additional_submit";s:0:"";s:17:"webformcomponents";a:4:{i:1204035579;a:9:{s:3:"cid";s:10:"1204035579";s:8:"form_key";s:9:"textfield";s:4:"name";s:9:"Team Name";s:4:"type";s:9:"textfield";s:5:"value";s:0:"";s:5:"extra";a:3:{s:11:"description";s:32:"Provide a name for the new team.";s:9:"maxlength";s:3:"256";s:5:"width";s:2:"50";}s:9:"mandatory";s:1:"1";s:6:"parent";s:1:"0";s:6:"weight";s:1:"0";}i:1204035840;a:9:{s:3:"cid";s:10:"1204035840";s:8:"form_key";s:5:"email";s:4:"name";s:19:"Your e-mail address";s:4:"type";s:5:"email";s:5:"value";s:10:"user email";s:5:"extra";a:2:{s:11:"description";s:25:"Enter your e-mail address";s:5:"width";s:2:"50";}s:9:"mandatory";s:1:"1";s:6:"parent";s:1:"0";s:6:"weight";s:1:"0";}i:1204035706;a:9:{s:3:"cid";s:10:"1204035706";s:8:"form_key";s:8:"textarea";s:4:"name";s:12:"Team Members";s:4:"type";s:8:"textarea";s:5:"value";s:0:"";s:5:"extra";a:3:{s:11:"description";s:70:"Provide the team member names and e-mail address, one member per line.";s:4:"cols";s:2:"70";s:4:"rows";s:2:"20";}s:9:"mandatory";s:1:"1";s:6:"parent";s:1:"0";s:6:"weight";s:1:"1";}i:1204036214;a:9:{s:3:"cid";s:10:"1204036214";s:8:"form_key";s:10:"textarea_1";s:4:"name";s:7:"Message";s:4:"type";s:8:"textarea";s:5:"value";s:0:"";s:5:"extra";a:3:{s:11:"description";s:80:"Provide any details here that will help the administrator complete your request.";s:4:"cols";s:2:"70";s:4:"rows";s:2:"15";}s:9:"mandatory";s:1:"0";s:6:"parent";s:1:"0";s:6:"weight";s:1:"2";}}s:13:"print_display";i:1;s:21:"print_display_comment";i:0;s:21:"print_display_urllist";i:1;s:22:"last_comment_timestamp";s:10:"1204035995";s:17:"last_comment_name";N;s:13:"comment_count";s:1:"0";s:8:"taxonomy";a:0:{}s:5:"files";a:0:{}s:14:"field_userlist";a:1:{i:0;a:1:{s:3:"uid";N;}}}',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'MNSapiNodeProcessor',
      'config' => array(
        'content_type' => 'webform',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'vid',
            'target' => 'vid',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'uid',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['webform'] = $feeds_importer;

  return $export;
}
