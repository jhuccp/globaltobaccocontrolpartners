<?php
/**
 * @file
 * jhu_feature_partner_organization.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_partner_organization_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function jhu_feature_partner_organization_node_info() {
  $items = array(
    'partner' => array(
      'name' => t('Partner Organization'),
      'base' => 'node_content',
      'description' => t('Select the organization that you work for.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
