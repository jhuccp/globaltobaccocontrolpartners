<?php
/**
 * @file
 * jhu_feature_library.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function jhu_feature_library_views_default_views() {
  $export = array();

  $view = new view;
  $view->name = 'library_search';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'node';
  $view->human_name = 'Library Search';
  $view->core = 7;
  $view->api_version = '3.0-alpha1';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Library';
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    2 => '2',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['query']['options']['query_comment'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['submit_button'] = 'Submit';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = 0;
  $handler->display->display_options['exposed_form']['options']['autosubmit_hide'] = 1;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '20';
  $handler->display->display_options['style_plugin'] = 'table';
  /* Header: Global: Text area */
  $handler->display->display_options['header']['area']['id'] = 'area';
  $handler->display->display_options['header']['area']['table'] = 'views';
  $handler->display->display_options['header']['area']['field'] = 'area';
  $handler->display->display_options['header']['area']['empty'] = FALSE;
  $handler->display->display_options['header']['area']['content'] = '<?php
global $pager_page_array, $pager_total, $pager_total_items, $pager_limits;

$current_page = $pager_page_array[0];
$total_pages = $pager_total[0];
$total_items = $pager_total_items[0];
$pager_limit = $pager_limits[0];

$start_item = $current_page * $pager_limit + 1;
$end_item = ($current_page + 1) * $pager_limit;
if ($current_page + 1 == $total_pages) {
  $end_item = $total_items;
}
print \'<span class="views-results-summary">\' . t(\'Search results [@start/@end of @total]\', array(\'@start\' => $start_item, \'@end\' => $end_item, \'@total\' => $total_items)) . \'</span>\';
?>';
  $handler->display->display_options['header']['area']['format'] = 'php_code';
  $handler->display->display_options['header']['area']['tokenize'] = 0;
  /* Footer: Global: Text area */
  $handler->display->display_options['footer']['area']['id'] = 'area';
  $handler->display->display_options['footer']['area']['table'] = 'views';
  $handler->display->display_options['footer']['area']['field'] = 'area';
  $handler->display->display_options['footer']['area']['empty'] = FALSE;
  $handler->display->display_options['footer']['area']['content'] = '<?php
if (user_access(\'create library content\')) {
  print l(t(\'Add to Library\'), \'node/add/library\', array(\'attributes\' => array(\'class\' => array(\'add-library-link\')))) . \'</br>\';
}
?>

<?php print l(t(\'Go Back to Library\'), \'node/2\'); ?>

';
  $handler->display->display_options['footer']['area']['format'] = 'php_code';
  $handler->display->display_options['footer']['area']['tokenize'] = 0;
  /* Field: Content: Title */
  $handler->display->display_options['fields']['title']['id'] = 'title';
  $handler->display->display_options['fields']['title']['table'] = 'node';
  $handler->display->display_options['fields']['title']['field'] = 'title';
  $handler->display->display_options['fields']['title']['label'] = 'Name';
  $handler->display->display_options['fields']['title']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['title']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['title']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['title']['alter']['external'] = 0;
  $handler->display->display_options['fields']['title']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['title']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['title']['alter']['word_boundary'] = 0;
  $handler->display->display_options['fields']['title']['alter']['ellipsis'] = 0;
  $handler->display->display_options['fields']['title']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['title']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['title']['alter']['html'] = 0;
  $handler->display->display_options['fields']['title']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['title']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['title']['hide_empty'] = 0;
  $handler->display->display_options['fields']['title']['empty_zero'] = 0;
  $handler->display->display_options['fields']['title']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['title']['link_to_node'] = 1;
  /* Field: Content: Post date */
  $handler->display->display_options['fields']['created']['id'] = 'created';
  $handler->display->display_options['fields']['created']['table'] = 'node';
  $handler->display->display_options['fields']['created']['field'] = 'created';
  $handler->display->display_options['fields']['created']['label'] = 'Date';
  $handler->display->display_options['fields']['created']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['created']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['created']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['created']['alter']['external'] = 0;
  $handler->display->display_options['fields']['created']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['created']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['created']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['created']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['created']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['created']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['created']['alter']['html'] = 0;
  $handler->display->display_options['fields']['created']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['created']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['created']['hide_empty'] = 0;
  $handler->display->display_options['fields']['created']['empty_zero'] = 0;
  $handler->display->display_options['fields']['created']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['created']['date_format'] = 'custom';
  $handler->display->display_options['fields']['created']['custom_date_format'] = 'd/m/Y';
  /* Field: Content: Type */
  $handler->display->display_options['fields']['field_library_type']['id'] = 'field_library_type';
  $handler->display->display_options['fields']['field_library_type']['table'] = 'field_data_field_library_type';
  $handler->display->display_options['fields']['field_library_type']['field'] = 'field_library_type';
  $handler->display->display_options['fields']['field_library_type']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['external'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['field_library_type']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['field_library_type']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['field_library_type']['alter']['html'] = 0;
  $handler->display->display_options['fields']['field_library_type']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['field_library_type']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['field_library_type']['hide_empty'] = 0;
  $handler->display->display_options['fields']['field_library_type']['empty_zero'] = 0;
  $handler->display->display_options['fields']['field_library_type']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['field_library_type']['type'] = 'taxonomy_term_reference_plain';
  $handler->display->display_options['fields']['field_library_type']['group_rows'] = 1;
  $handler->display->display_options['fields']['field_library_type']['delta_offset'] = '0';
  $handler->display->display_options['fields']['field_library_type']['delta_reversed'] = 0;
  $handler->display->display_options['fields']['field_library_type']['field_api_classes'] = 0;
  /* Field: Content: Body */
  $handler->display->display_options['fields']['body']['id'] = 'body';
  $handler->display->display_options['fields']['body']['table'] = 'field_data_body';
  $handler->display->display_options['fields']['body']['field'] = 'body';
  $handler->display->display_options['fields']['body']['label'] = 'Description';
  $handler->display->display_options['fields']['body']['alter']['alter_text'] = 0;
  $handler->display->display_options['fields']['body']['alter']['make_link'] = 0;
  $handler->display->display_options['fields']['body']['alter']['absolute'] = 0;
  $handler->display->display_options['fields']['body']['alter']['external'] = 0;
  $handler->display->display_options['fields']['body']['alter']['replace_spaces'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim_whitespace'] = 0;
  $handler->display->display_options['fields']['body']['alter']['nl2br'] = 0;
  $handler->display->display_options['fields']['body']['alter']['word_boundary'] = 1;
  $handler->display->display_options['fields']['body']['alter']['ellipsis'] = 1;
  $handler->display->display_options['fields']['body']['alter']['strip_tags'] = 0;
  $handler->display->display_options['fields']['body']['alter']['trim'] = 0;
  $handler->display->display_options['fields']['body']['alter']['html'] = 0;
  $handler->display->display_options['fields']['body']['element_label_colon'] = 1;
  $handler->display->display_options['fields']['body']['element_default_classes'] = 1;
  $handler->display->display_options['fields']['body']['hide_empty'] = 0;
  $handler->display->display_options['fields']['body']['empty_zero'] = 0;
  $handler->display->display_options['fields']['body']['hide_alter_empty'] = 0;
  $handler->display->display_options['fields']['body']['type'] = 'text_summary_or_trimmed';
  $handler->display->display_options['fields']['body']['settings'] = array(
    'trim_length' => '600',
  );
  $handler->display->display_options['fields']['body']['field_api_classes'] = 0;
  /* Sort criterion: Content: Post date */
  $handler->display->display_options['sorts']['created']['id'] = 'created';
  $handler->display->display_options['sorts']['created']['table'] = 'node';
  $handler->display->display_options['sorts']['created']['field'] = 'created';
  $handler->display->display_options['sorts']['created']['order'] = 'DESC';
  /* Filter criterion: Content: Published */
  $handler->display->display_options['filters']['status']['id'] = 'status';
  $handler->display->display_options['filters']['status']['table'] = 'node';
  $handler->display->display_options['filters']['status']['field'] = 'status';
  $handler->display->display_options['filters']['status']['value'] = 1;
  $handler->display->display_options['filters']['status']['group'] = 0;
  $handler->display->display_options['filters']['status']['expose']['operator'] = FALSE;
  /* Filter criterion: Content: Type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'node';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'library' => 'library',
  );
  /* Filter criterion: Search: Search Terms */
  $handler->display->display_options['filters']['keys']['id'] = 'keys';
  $handler->display->display_options['filters']['keys']['table'] = 'search_index';
  $handler->display->display_options['filters']['keys']['field'] = 'keys';
  $handler->display->display_options['filters']['keys']['exposed'] = TRUE;
  $handler->display->display_options['filters']['keys']['expose']['operator_id'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['label'] = 'Keywords:';
  $handler->display->display_options['filters']['keys']['expose']['operator'] = 'keys_op';
  $handler->display->display_options['filters']['keys']['expose']['identifier'] = 'keys';
  $handler->display->display_options['filters']['keys']['expose']['multiple'] = FALSE;
  /* Filter criterion: Content: Region (field_library_region) */
  $handler->display->display_options['filters']['field_library_region_tid']['id'] = 'field_library_region_tid';
  $handler->display->display_options['filters']['field_library_region_tid']['table'] = 'field_data_field_library_region';
  $handler->display->display_options['filters']['field_library_region_tid']['field'] = 'field_library_region_tid';
  $handler->display->display_options['filters']['field_library_region_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_region_tid']['expose']['operator_id'] = 'field_library_region_tid_op';
  $handler->display->display_options['filters']['field_library_region_tid']['expose']['label'] = 'Region:';
  $handler->display->display_options['filters']['field_library_region_tid']['expose']['operator'] = 'field_library_region_tid_op';
  $handler->display->display_options['filters']['field_library_region_tid']['expose']['identifier'] = 'region';
  $handler->display->display_options['filters']['field_library_region_tid']['expose']['reduce'] = 0;
  $handler->display->display_options['filters']['field_library_region_tid']['reduce_duplicates'] = 0;
  $handler->display->display_options['filters']['field_library_region_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_library_region_tid']['vocabulary'] = 'region';
  $handler->display->display_options['filters']['field_library_region_tid']['error_message'] = 0;
  /* Filter criterion: Content: Country (field_library_country) */
  $handler->display->display_options['filters']['field_library_country_tid']['id'] = 'field_library_country_tid';
  $handler->display->display_options['filters']['field_library_country_tid']['table'] = 'field_data_field_library_country';
  $handler->display->display_options['filters']['field_library_country_tid']['field'] = 'field_library_country_tid';
  $handler->display->display_options['filters']['field_library_country_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_country_tid']['expose']['operator_id'] = 'field_library_country_tid_op';
  $handler->display->display_options['filters']['field_library_country_tid']['expose']['label'] = 'Countries:';
  $handler->display->display_options['filters']['field_library_country_tid']['expose']['operator'] = 'field_library_country_tid_op';
  $handler->display->display_options['filters']['field_library_country_tid']['expose']['identifier'] = 'country';
  $handler->display->display_options['filters']['field_library_country_tid']['expose']['reduce'] = 0;
  $handler->display->display_options['filters']['field_library_country_tid']['reduce_duplicates'] = 0;
  $handler->display->display_options['filters']['field_library_country_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_library_country_tid']['vocabulary'] = 'country';
  $handler->display->display_options['filters']['field_library_country_tid']['error_message'] = 1;
  /* Filter criterion: Content: Type (field_library_type) */
  $handler->display->display_options['filters']['field_library_type_tid']['id'] = 'field_library_type_tid';
  $handler->display->display_options['filters']['field_library_type_tid']['table'] = 'field_data_field_library_type';
  $handler->display->display_options['filters']['field_library_type_tid']['field'] = 'field_library_type_tid';
  $handler->display->display_options['filters']['field_library_type_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_type_tid']['expose']['operator_id'] = 'field_library_type_tid_op';
  $handler->display->display_options['filters']['field_library_type_tid']['expose']['label'] = 'Type:';
  $handler->display->display_options['filters']['field_library_type_tid']['expose']['operator'] = 'field_library_type_tid_op';
  $handler->display->display_options['filters']['field_library_type_tid']['expose']['identifier'] = 'type';
  $handler->display->display_options['filters']['field_library_type_tid']['expose']['reduce'] = 0;
  $handler->display->display_options['filters']['field_library_type_tid']['reduce_duplicates'] = 0;
  $handler->display->display_options['filters']['field_library_type_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_library_type_tid']['vocabulary'] = 'type';
  $handler->display->display_options['filters']['field_library_type_tid']['error_message'] = 1;
  /* Filter criterion: Content: Topic (field_library_topic) */
  $handler->display->display_options['filters']['field_library_topic_tid']['id'] = 'field_library_topic_tid';
  $handler->display->display_options['filters']['field_library_topic_tid']['table'] = 'field_data_field_library_topic';
  $handler->display->display_options['filters']['field_library_topic_tid']['field'] = 'field_library_topic_tid';
  $handler->display->display_options['filters']['field_library_topic_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_topic_tid']['expose']['operator_id'] = 'field_library_topic_tid_op';
  $handler->display->display_options['filters']['field_library_topic_tid']['expose']['label'] = 'Topic:';
  $handler->display->display_options['filters']['field_library_topic_tid']['expose']['operator'] = 'field_library_topic_tid_op';
  $handler->display->display_options['filters']['field_library_topic_tid']['expose']['identifier'] = 'topic';
  $handler->display->display_options['filters']['field_library_topic_tid']['expose']['reduce'] = 0;
  $handler->display->display_options['filters']['field_library_topic_tid']['reduce_duplicates'] = 0;
  $handler->display->display_options['filters']['field_library_topic_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_library_topic_tid']['vocabulary'] = 'topic';
  $handler->display->display_options['filters']['field_library_topic_tid']['error_message'] = 1;
  /* Filter criterion: Content: Language (field_library_language) */
  $handler->display->display_options['filters']['field_library_language_tid']['id'] = 'field_library_language_tid';
  $handler->display->display_options['filters']['field_library_language_tid']['table'] = 'field_data_field_library_language';
  $handler->display->display_options['filters']['field_library_language_tid']['field'] = 'field_library_language_tid';
  $handler->display->display_options['filters']['field_library_language_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_library_language_tid']['expose']['operator_id'] = 'field_library_language_tid_op';
  $handler->display->display_options['filters']['field_library_language_tid']['expose']['label'] = 'Language:';
  $handler->display->display_options['filters']['field_library_language_tid']['expose']['operator'] = 'field_library_language_tid_op';
  $handler->display->display_options['filters']['field_library_language_tid']['expose']['identifier'] = 'language';
  $handler->display->display_options['filters']['field_library_language_tid']['expose']['reduce'] = 0;
  $handler->display->display_options['filters']['field_library_language_tid']['reduce_duplicates'] = 0;
  $handler->display->display_options['filters']['field_library_language_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_library_language_tid']['vocabulary'] = 'language';
  $handler->display->display_options['filters']['field_library_language_tid']['error_message'] = 1;

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['exposed_block'] = TRUE;
  $handler->display->display_options['path'] = 'library-search';
  $export['library_search'] = $view;

  return $export;
}
