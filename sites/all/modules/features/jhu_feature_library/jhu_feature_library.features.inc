<?php
/**
 * @file
 * jhu_feature_library.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_library_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function jhu_feature_library_views_api() {
  list($module, $api) = func_get_args();
  if ($module == "views" && $api == "views_default") {
    return array("version" => "3.0");
  }
}

/**
 * Implements hook_node_info().
 */
function jhu_feature_library_node_info() {
  $items = array(
    'library' => array(
      'name' => t('Library document'),
      'base' => 'node_content',
      'description' => t('Materials that belong in the library.'),
      'has_title' => '1',
      'title_label' => t('Title of the document'),
      'help' => t('Please fill in all the fields as completely as possible. The red asterisk marks required fields.  Use the established <a href="/sites/default/files/lib_title_guidelines.pdf">naming conventions</a> for the titles of documents.'),
    ),
  );
  return $items;
}
