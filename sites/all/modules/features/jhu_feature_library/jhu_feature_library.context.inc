<?php
/**
 * @file
 * jhu_feature_library.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function jhu_feature_library_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'library';
  $context->description = '';
  $context->tag = 'library';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'library_documents' => 'library_documents',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views--exp-library_search-page' => array(
          'module' => 'views',
          'delta' => '-exp-library_search-page',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('library');
  $export['library'] = $context;

  return $export;
}
