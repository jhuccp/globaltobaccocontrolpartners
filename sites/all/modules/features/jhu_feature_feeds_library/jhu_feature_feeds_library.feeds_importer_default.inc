<?php
/**
 * @file
 * jhu_feature_feeds_library.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_library_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'library';
  $feeds_importer->config = array(
    'name' => 'Library',
    'description' => 'Import Library Document nodes',
    'fetcher' => array(
      'plugin_key' => 'MNSapiFetcher',
      'config' => array(
        'list_url' => 'mn_sapi/list/node/library',
        'get_url' => 'mn_sapi/get/node/[id]',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MNSapiParser',
      'config' => array(
        'serialized_example' => 'O:8:"stdClass":29:{s:3:"nid";s:4:"2628";s:3:"vid";s:4:"2664";s:4:"type";s:7:"library";s:6:"status";s:1:"1";s:7:"created";s:10:"1316020263";s:7:"changed";s:10:"1316020263";s:7:"comment";s:1:"0";s:7:"promote";s:1:"0";s:6:"sticky";s:1:"0";s:18:"revision_timestamp";s:10:"1316020263";s:5:"title";s:32:"JHSPH Monthly Update August 2011";s:4:"body";s:64:"Brief discussion of JHSPH-BI-related activities for August 2011.";s:6:"teaser";s:64:"Brief discussion of JHSPH-BI-related activities for August 2011.";s:3:"log";s:0:"";s:6:"format";s:1:"1";s:3:"uid";s:3:"123";s:4:"name";s:8:"stamplin";s:7:"picture";s:30:"files/pictures/picture-123.png";s:4:"data";s:311:"a:10:{s:7:"contact";i:1;s:17:"mimemail_textonly";i:0;s:8:"og_email";s:1:"2";s:14:"picture_delete";i:0;s:14:"picture_upload";s:0:"";s:5:"block";a:1:{s:5:"event";a:1:{i:1;i:0;}}s:19:"mass_contact_optout";i:0;s:16:"privatemsg_allow";i:1;s:28:"privatemsg_setmessage_notify";i:1;s:20:"privatemsg_mailalert";s:1:"0";}";s:13:"print_display";i:1;s:21:"print_display_comment";i:0;s:21:"print_display_urllist";i:1;s:22:"last_comment_timestamp";s:10:"1316020263";s:17:"last_comment_name";N;s:13:"comment_count";s:1:"0";s:8:"taxonomy";a:5:{i:87;O:8:"stdClass":5:{s:3:"tid";s:2:"87";s:3:"vid";s:1:"5";s:4:"name";s:34:"Bloomberg Priority Countries (15)*";s:11:"description";s:0:"";s:6:"weight";s:2:"-9";}i:114;O:8:"stdClass":5:{s:3:"tid";s:3:"114";s:3:"vid";s:2:"11";s:4:"name";s:12:"Non-specific";s:11:"description";s:0:"";s:6:"weight";s:2:"-9";}i:373;O:8:"stdClass":5:{s:3:"tid";s:3:"373";s:3:"vid";s:2:"13";s:4:"name";s:7:"English";s:11:"description";s:0:"";s:6:"weight";s:1:"0";}i:1112;O:8:"stdClass":5:{s:3:"tid";s:4:"1112";s:3:"vid";s:1:"6";s:4:"name";s:14:"Monthly Update";s:11:"description";s:50:"Monthly progress reports from Bloomberg partners. ";s:6:"weight";s:1:"0";}i:1148;O:8:"stdClass":5:{s:3:"tid";s:4:"1148";s:3:"vid";s:2:"12";s:4:"name";s:15:"Tobacco control";s:11:"description";s:0:"";s:6:"weight";s:1:"0";}}s:18:"field_submitted_by";a:1:{i:0;a:1:{s:5:"value";s:13:"Steve Tamplin";}}s:26:"field_partner_organization";a:1:{i:0;a:1:{s:5:"value";s:1:"3";}}s:23:"field_download_document";a:1:{i:0;a:8:{s:3:"fid";s:4:"4207";s:11:"description";s:42:"JHSPH Monthly Update August 2011_Final.doc";s:4:"list";s:1:"1";s:3:"nid";s:4:"2628";s:8:"filename";s:42:"JHSPH Monthly Update August 2011_Final.doc";s:8:"filepath";s:48:"files/JHSPH Monthly Update August 2011_Final.doc";s:8:"filemime";s:18:"application/msword";s:8:"filesize";s:5:"73216";}}}',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'JHULibraryNodeProcessor',
      'config' => array(
        'content_type' => 'library',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'vid',
            'target' => 'vid',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'created',
            'target' => 'created',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'changed',
            'target' => 'changed',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'status',
            'target' => 'status',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'uid',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'taxonomy',
            'target' => 'field_library_topic',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'taxonomy',
            'target' => 'field_library_region',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'taxonomy',
            'target' => 'field_library_language',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'taxonomy',
            'target' => 'field_library_type',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'taxonomy',
            'target' => 'field_library_country',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'field_submitted_by',
            'target' => 'field_library_submittedby',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'field_partner_organization',
            'target' => 'field_library_partner',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'field_download_document',
            'target' => 'field_library_uploaddocument',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['library'] = $feeds_importer;

  return $export;
}
