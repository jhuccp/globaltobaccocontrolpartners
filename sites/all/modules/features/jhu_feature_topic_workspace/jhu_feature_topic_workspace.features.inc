<?php
/**
 * @file
 * jhu_feature_topic_workspace.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_topic_workspace_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function jhu_feature_topic_workspace_node_info() {
  $items = array(
    'topic_workspace' => array(
      'name' => t('Topic Workspace'),
      'base' => 'node_content',
      'description' => t('This is the topic workspace type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
