<?php
/**
 * @file
 * jhu_feature_country.ds.inc
 */

/**
 * Implements hook_ds_field_settings_info().
 */
function jhu_feature_country_ds_field_settings_info() {
  $export = array();

  $ds_fieldsetting = new stdClass();
  $ds_fieldsetting->api_version = 1;
  $ds_fieldsetting->id = 'node|country|full';
  $ds_fieldsetting->entity_type = 'node';
  $ds_fieldsetting->bundle = 'country';
  $ds_fieldsetting->view_mode = 'full';
  $ds_fieldsetting->settings = array(
    'country_latest_library_resources' => array(
      'weight' => '1',
      'label' => 'hidden',
      'format' => 'default',
    ),
    'country_workspace_link' => array(
      'weight' => '2',
      'label' => 'hidden',
      'format' => 'default',
    ),
  );
  $export['node|country|full'] = $ds_fieldsetting;

  return $export;
}

/**
 * Implements hook_ds_custom_fields_info().
 */
function jhu_feature_country_ds_custom_fields_info() {
  $export = array();

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'country_latest_library_resources';
  $ds_field->label = 'country_library_resources';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->properties = array(
    'block' => 'views|8284f148053ff6bd5dce07b703e80b6e',
    'block_render' => '1',
  );
  $export['country_latest_library_resources'] = $ds_field;

  $ds_field = new stdClass();
  $ds_field->api_version = 1;
  $ds_field->field = 'country_workspace_link';
  $ds_field->label = 'country_workspace_link';
  $ds_field->field_type = 6;
  $ds_field->entities = array(
    'node' => 'node',
  );
  $ds_field->properties = array(
    'block' => 'views|country_workspace_link-block',
    'block_render' => '1',
  );
  $export['country_workspace_link'] = $ds_field;

  return $export;
}

/**
 * Implements hook_ds_layout_settings_info().
 */
function jhu_feature_country_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|country|full';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'country';
  $ds_layout->view_mode = 'full';
  $ds_layout->layout = 'ds_1col';
  $ds_layout->settings = array(
    'hide_empty_regions' => 0,
    'hide_sidebars' => 0,
    'regions' => array(
      'ds_content' => array(
        0 => 'body',
        1 => 'country_latest_library_resources',
        2 => 'country_workspace_link',
      ),
    ),
    'fields' => array(
      'body' => 'ds_content',
      'country_latest_library_resources' => 'ds_content',
      'country_workspace_link' => 'ds_content',
    ),
    'classes' => array(),
  );
  $export['node|country|full'] = $ds_layout;

  return $export;
}
