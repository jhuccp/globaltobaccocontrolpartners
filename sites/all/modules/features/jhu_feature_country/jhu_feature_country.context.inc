<?php
/**
 * @file
 * jhu_feature_country.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function jhu_feature_country_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'country_node';
  $context->description = '';
  $context->tag = 'Country';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'country' => 'country',
      ),
      'options' => array(
        'node_form' => '1',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-8284f148053ff6bd5dce07b703e80b6e' => array(
          'module' => 'views',
          'delta' => '8284f148053ff6bd5dce07b703e80b6e',
          'region' => 'content',
          'weight' => '-47',
        ),
        'views-country_workspace_link-block' => array(
          'module' => 'views',
          'delta' => 'country_workspace_link-block',
          'region' => 'content',
          'weight' => '-46',
        ),
        'block-11' => array(
          'module' => 'block',
          'delta' => 11,
          'region' => 'content',
          'weight' => '-45',
        ),
        'views-6af5d11c9383c022bddcafef802dc971' => array(
          'module' => 'views',
          'delta' => '6af5d11c9383c022bddcafef802dc971',
          'region' => 'content',
          'weight' => '-44',
        ),
        'views-country_latest_from_forum-block' => array(
          'module' => 'views',
          'delta' => 'country_latest_from_forum-block',
          'region' => 'content',
          'weight' => '-43',
        ),
        'views-country_statistics-block' => array(
          'module' => 'views',
          'delta' => 'country_statistics-block',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Country');
  $export['country_node'] = $context;

  return $export;
}
