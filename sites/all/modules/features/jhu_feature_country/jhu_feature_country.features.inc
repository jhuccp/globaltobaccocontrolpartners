<?php
/**
 * @file
 * jhu_feature_country.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_country_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function jhu_feature_country_node_info() {
  $items = array(
    'country' => array(
      'name' => t('Country Overview Pages'),
      'base' => 'node_content',
      'description' => t('This is the page that displays an overview of all the information available on a country when that country is clicked on in the Country Overview Map.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
