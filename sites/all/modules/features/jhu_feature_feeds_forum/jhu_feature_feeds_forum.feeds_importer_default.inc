<?php
/**
 * @file
 * jhu_feature_feeds_forum.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_forum_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'forum';
  $feeds_importer->config = array(
    'name' => 'Forum',
    'description' => 'Import Forum nodes',
    'fetcher' => array(
      'plugin_key' => 'MNSapiFetcher',
      'config' => array(
        'list_url' => 'mn_sapi/list/node/forum',
        'get_url' => 'mn_sapi/get/node/[id]',
      ),
    ),
    'parser' => array(
      'plugin_key' => 'MNSapiParser',
      'config' => array(
        'serialized_example' => 'O:8:"stdClass":28:{s:3:"nid";s:4:"1045";s:3:"vid";s:4:"1045";s:4:"type";s:5:"forum";s:6:"status";s:1:"1";s:7:"created";s:10:"1204741763";s:7:"changed";s:10:"1204741763";s:7:"comment";s:1:"2";s:7:"promote";s:1:"0";s:6:"sticky";s:1:"0";s:18:"revision_timestamp";s:10:"1204741763";s:5:"title";s:29:"Agenda for Upcoming Training ";s:4:"body";s:256:"Attached please find the draft schedule for the March 24 - April 4 training. We will be using this draft as the basis of discussion for our India training committee call on Thursday morning at 8:00 EST. Please make suggestions and comments. Thanks, Heather";s:6:"teaser";s:256:"Attached please find the draft schedule for the March 24 - April 4 training. We will be using this draft as the basis of discussion for our India training committee call on Thursday morning at 8:00 EST. Please make suggestions and comments. Thanks, Heather";s:3:"log";s:0:"";s:6:"format";s:1:"1";s:3:"uid";s:2:"22";s:4:"name";s:7:"hwipfli";s:7:"picture";s:29:"files/pictures/picture-22.jpg";s:4:"data";s:384:"a:12:{s:7:"contact";i:1;s:17:"mimemail_textonly";i:0;s:8:"og_email";s:1:"2";s:14:"picture_delete";i:0;s:14:"picture_upload";s:0:"";s:5:"block";a:1:{s:5:"event";a:1:{i:1;i:0;}}s:10:"skype_name";s:0:"";s:22:"available_skype_status";a:1:{i:7;s:1:"7";}s:19:"mass_contact_optout";i:0;s:16:"privatemsg_allow";i:1;s:28:"privatemsg_setmessage_notify";i:1;s:20:"privatemsg_mailalert";s:1:"0";}";s:3:"tid";s:3:"103";s:13:"print_display";i:1;s:21:"print_display_comment";i:0;s:21:"print_display_urllist";i:1;s:22:"last_comment_timestamp";s:10:"1204741854";s:17:"last_comment_name";s:0:"";s:13:"comment_count";s:1:"1";s:8:"taxonomy";a:1:{i:103;O:8:"stdClass":5:{s:3:"tid";s:3:"103";s:3:"vid";s:2:"10";s:4:"name";s:18:"India Country Team";s:11:"description";s:73:"This forum is for discussion of issues related to the India Country Team.";s:6:"weight";s:1:"0";}}s:5:"files";a:0:{}}',
      ),
    ),
    'processor' => array(
      'plugin_key' => 'MNSapiNodeProcessor',
      'config' => array(
        'content_type' => 'forum',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'vid',
            'target' => 'vid',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'created',
            'target' => 'created',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'uid',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'taxonomy',
            'target' => 'taxonomy_forums',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '1',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['forum'] = $feeds_importer;

  return $export;
}
