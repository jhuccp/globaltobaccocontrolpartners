<?php
/**
 * @file
 * jhu_feature_forum.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function jhu_feature_forum_context_default_contexts() {
  $export = array();

  $context = new stdClass;
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'forum_node';
  $context->description = '';
  $context->tag = 'forum';
  $context->conditions = array(
    'node' => array(
      'values' => array(
        'forum' => 'forum',
      ),
      'options' => array(
        'node_form' => '0',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'block-3' => array(
          'module' => 'block',
          'delta' => 3,
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
        'nodeblock-22' => array(
          'module' => 'nodeblock',
          'delta' => 22,
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
        'user-online' => array(
          'module' => 'user',
          'delta' => 'online',
          'region' => 'sidebar_second',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('forum');
  $export['forum_node'] = $context;

  return $export;
}
