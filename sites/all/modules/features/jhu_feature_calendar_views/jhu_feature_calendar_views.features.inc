<?php
/**
 * @file
 * jhu_feature_calendar_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function jhu_feature_calendar_views_views_api() {
  return array("version" => "3.0");
}
