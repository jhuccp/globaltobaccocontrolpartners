<?php
/**
 * @file
 * jhu_feature_development_progress_tracking.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_development_progress_tracking_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function jhu_feature_development_progress_tracking_node_info() {
  $items = array(
    'development_progress_tracking' => array(
      'name' => t('Development Progress Tracking'),
      'base' => 'node_content',
      'description' => t('This content type is only used to track progress of development on the site and should not be included in the site search or visible to normal users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
