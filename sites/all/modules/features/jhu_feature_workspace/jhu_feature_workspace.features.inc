<?php
/**
 * @file
 * jhu_feature_workspace.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_workspace_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function jhu_feature_workspace_node_info() {
  $items = array(
    'workspace' => array(
      'name' => t('Workspace'),
      'base' => 'node_content',
      'description' => t('This is the workspace type'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
