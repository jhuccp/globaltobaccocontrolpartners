<?php
/**
 * @file
 * jhu_feature_tasks.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_tasks_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function jhu_feature_tasks_node_info() {
  $items = array(
    'tasks' => array(
      'name' => t('Task'),
      'base' => 'node_content',
      'description' => t('These are the to-do tasks for team workspace.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
