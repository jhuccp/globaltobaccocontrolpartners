<?php
/**
 * @file
 * jhu_feature_feeds_events.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_events_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'events';
  $feeds_importer->config = array(
    'name' => 'Events',
    'description' => 'Import Event nodes',
    'fetcher' => array(
      'plugin_key' => 'JHUNodeFetcher',
      'config' => array(),
    ),
    'parser' => array(
      'plugin_key' => 'JHUNodeParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'JHUNodeProcessor',
      'config' => array(
        'content_type' => 'event',
        'expire' => '-1',
        'author' => 0,
        'mappings' => array(
          0 => array(
            'source' => 'nid',
            'target' => 'nid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'vid',
            'target' => 'vid',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'uid',
            'target' => 'uid',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'changed',
            'target' => 'changed',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'date',
            'target' => 'event_date',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'created',
            'target' => 'created',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'field_event_submitted_by',
            'target' => 'field_event_submitted_by',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'taxonomy',
            'target' => 'field_event_type',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'taxonomy',
            'target' => 'field_event_region',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'taxonomy',
            'target' => 'field_event_partner',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'taxonomy',
            'target' => 'field_event_country',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'taxonomy',
            'target' => 'field_event_audience',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'taxonomy',
            'target' => 'field_event_topic',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'field_contact',
            'target' => 'field_event_contact',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'field_provisional',
            'target' => 'field_event_dateconfirmed',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'field_draft_agenda',
            'target' => 'field_event_draftagenda',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'field_working_documents',
            'target' => 'field_event_eventdocuments',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'field_invited_participants',
            'target' => 'field_event_invitedparticipants',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => '0',
        'input_format' => 'plain_text',
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['events'] = $feeds_importer;

  return $export;
}
