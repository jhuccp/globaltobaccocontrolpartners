<?php
/**
 * @file
 * jhu_feature_feeds_users.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function jhu_feature_feeds_users_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "feeds" && $api == "feeds_importer_default") {
    return array("version" => "1");
  }
}
