<?php
/**
 * @file
 * jhu_feature_feeds_users.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function jhu_feature_feeds_users_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass;
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'users';
  $feeds_importer->config = array(
    'name' => 'Users',
    'description' => '',
    'fetcher' => array(
      'plugin_key' => 'JHUUserFetcher',
      'config' => array(),
    ),
    'parser' => array(
      'plugin_key' => 'JHUUserParser',
      'config' => array(),
    ),
    'processor' => array(
      'plugin_key' => 'JHUUserProcessor',
      'config' => array(
        'roles' => array(),
        'status' => 1,
        'defuse_mail' => FALSE,
        'mappings' => array(
          0 => array(
            'source' => 'uid',
            'target' => 'uid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'name',
            'target' => 'name',
            'unique' => 0,
          ),
          2 => array(
            'source' => 'mail',
            'target' => 'mail',
            'unique' => 0,
          ),
          3 => array(
            'source' => 'pass',
            'target' => 'pass',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'status',
            'target' => 'status',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'created',
            'target' => 'created',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'access',
            'target' => 'access',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'timezone',
            'target' => 'timezone',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'profile_title',
            'target' => 'field_user_title',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'profile_firstname',
            'target' => 'field_user_firstname',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'profile_lastname',
            'target' => 'field_user_lastname',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'profile_phonenumber',
            'target' => 'field_user_phonenumber',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'profile_city',
            'target' => 'field_user_city',
            'unique' => FALSE,
          ),
          13 => array(
            'source' => 'profile_country',
            'target' => 'field_user_country',
            'unique' => FALSE,
          ),
          14 => array(
            'source' => 'profile_previousEd',
            'target' => 'field_user_previouseducation',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'profile_proInterests',
            'target' => 'field_user_bio',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'profile_partner_org',
            'target' => 'field_user_partnerorganization',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'profile_faxnumber',
            'target' => 'field_user_faxnumber',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'profile_address',
            'target' => 'field_user_address',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'profile_bprojects',
            'target' => 'field_user_bloombergprojects',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'picture',
            'target' => 'field_user_picture',
            'unique' => FALSE,
          ),
          21 => array(
            'source' => 'roles',
            'target' => 'roles',
            'unique' => FALSE,
          ),
        ),
        'update_existing' => 0,
        'input_format' => NULL,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => 1800,
    'expire_period' => 3600,
    'import_on_create' => TRUE,
    'process_in_background' => FALSE,
  );
  $export['users'] = $feeds_importer;

  return $export;
}
