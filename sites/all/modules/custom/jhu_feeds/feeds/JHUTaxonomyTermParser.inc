<?php

class JHUTaxonomyTermParser extends FeedsParser {

  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $result = new FeedsParserResult();
    $result->title = 'Taxonomy Terms';
    $result->description = 'D5 Taxonomy Terms';
    $result->link = '';
    $fetcher_result_content = $fetcher_result->getContent();
    if (is_array($fetcher_result_content)) {
      foreach ($fetcher_result_content as $d5term) {
        $result->items[$d5term->tid] = (array)$d5term;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($result, 'parser result');
    }
    return $result;
  }

  public function getMappingSources() {
    // Example of D5 term. We need it for keys.
    $item = array(
      'tid' => '12',
      'vid' => '10',
      'name' => 'Brazil Country Team',
      'description' => 'This forum is for discussion of issues related to the Brazil Country Team.',
      'weight' => '0',
      'parents' => array(),
    );

    // Go though all keys and prepare items.
    $ret = array();
    foreach (array_keys($item) as $key) {
      $ret[$key] = array(
        'name' => t('D5 @key', array('@key' => $key)),
        'description' => t('D5 description @key', array('@key' => $key)),
      );
    }
    return $ret + parent::getMappingSources();
  }
}
