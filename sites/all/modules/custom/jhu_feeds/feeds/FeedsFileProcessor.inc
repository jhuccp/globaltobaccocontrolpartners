<?php

/**
 * @file
 * Class definition of FeedsFileProcessor.
 */

/**
 * Creates files from feed items.
 */
class FeedsFileProcessor extends FeedsProcessor {
  /**
   * Define entity type.
   */
  public function entityType() {
    return 'file';
  }

  /**
   * Implements parent::entityInfo().
   */
  protected function entityInfo() {
    $info = parent::entityInfo();
    $info['label plural'] = t('Files');
    return $info;
  }

  /**
   * Creates a new file in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $item = func_get_arg(1);
    db_query('REPLACE INTO {file_managed} (fid) VALUES (:fid)', array(':fid' => $item['fid']));

    $file = new stdClass();
    $file->fid = NULL;
    $file->status = FILE_STATUS_PERMANENT;
    return $file;
  }

  /**
   * Loads an existing file.
   *
   * If the update existing method is not FEEDS_UPDATE_EXISTING, only the file
   * table will be loaded, foregoing the file_load API for better performance.
   */
  protected function entityLoad(FeedsSource $source, $fid) {
    if ($this->config['update_existing'] == FEEDS_UPDATE_EXISTING) {
      $file = file_load($fid);
    }
    else {
      // We're replacing the existing file. Only save the absolutely necessary.
      $file = db_query("SELECT * FROM {file_managed} WHERE fid = :fid", array(':fid' => $fid))->fetchObject();
      $file->uid = $this->config['author'];
    }

    return $file;
  }

  /**
   * Save a file.
   */
  public function entitySave($entity) {
    file_save($entity);
  }

  /**
   * Delete a series of files.
   */
  protected function entityDeleteMultiple($fids) {
    foreach ($fids as $fid) {
      $file = file_load($fid);
      file_delete($file);
    }
  }

  /**
   * Implement expire().
   *
   * @todo: move to processor stage?
   */
  public function expire($time = NULL) {
    if ($time === NULL) {
      $time = $this->expiryTime();
    }
    if ($time == FEEDS_EXPIRE_NEVER) {
      return;
    }
    $count = $this->getLimit();
    $files = db_query_range("SELECT f.fid FROM {file_managed} f JOIN {feeds_item} fi ON fi.entity_type = 'file' AND f.fid = fi.entity_id WHERE fi.id = :id AND f.timestamp < :created", 0, $count, array(':id' => $this->id, ':created' => REQUEST_TIME - $time));
    $fids = array();
    foreach ($files as $file) {
      $fids[$file->fid] = $file->fid;
    }
    $this->entityDeleteMultiple($fids);
    if (db_query_range("SELECT 1 FROM {file_managed} f JOIN {feeds_item} fi ON fi.entity_type = 'file' AND f.fid = fi.entity_id WHERE fi.id = :id AND f.timestamp < :created", 0, 1, array(':id' => $this->id, ':created' => REQUEST_TIME - $time))->fetchField()) {
      return FEEDS_BATCH_ACTIVE;
    }
    return FEEDS_BATCH_COMPLETE;
  }

  /**
   * Return expiry time.
   */
  public function expiryTime() {
    return $this->config['expire'];
  }

  /**
   * Override parent::configDefaults().
   */
  public function configDefaults() {
    return array(
      'expire' => FEEDS_EXPIRE_NEVER,
      'author' => 0,
    ) + parent::configDefaults();
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    $author = user_load($this->config['author']);
    $form['author'] = array(
      '#type' => 'textfield',
      '#title' => t('Author'),
      '#description' => t('Select the author of the files to be created - leave empty to assign "anonymous".'),
      '#autocomplete_path' => 'user/autocomplete',
      '#default_value' => empty($author->name) ?  'anonymous' : check_plain($author->name),
    );
    $period = drupal_map_assoc(array(FEEDS_EXPIRE_NEVER, 3600, 10800, 21600, 43200, 86400, 259200, 604800, 2592000, 2592000 * 3, 2592000 * 6, 31536000), 'feeds_format_expire');
    $form['expire'] = array(
      '#type' => 'select',
      '#title' => t('Expire files'),
      '#options' => $period,
      '#description' => t('Select after how much time files should be deleted. The file\'s published date will be used for determining the file\'s age, see Mapping settings.'),
      '#default_value' => $this->config['expire'],
    );
    $form['update_existing']['#options'] = array(
      FEEDS_SKIP_EXISTING => 'Do not update existing files',
      FEEDS_REPLACE_EXISTING => 'Replace existing files',
      FEEDS_UPDATE_EXISTING => 'Update existing files (slower than replacing them)',
    );
    return $form;
  }

  /**
   * Override parent::configFormValidate().
   */
  public function configFormValidate(&$values) {
    if ($author = user_load_by_name($values['author'])) {
      $values['author'] = $author->uid;
    }
    else {
      $values['author'] = 0;
    }
  }

  /**
   * Reschedule if expiry time changes.
   */
  public function configFormSubmit(&$values) {
    if ($this->config['expire'] != $values['expire']) {
      feeds_reschedule($this->id);
    }
    parent::configFormSubmit($values);
  }

  /**
   * Override setTargetElement to operate on a target item that is a file.
   */
  public function setTargetElement(FeedsSource $source, $target_file, $target_element, $value) {
    switch ($target_element) {
      case 'created':
        $target_file->timestamp = feeds_to_unixtime($value, REQUEST_TIME);
        break;
      case 'feeds_source':
        // Get the class of the feed file importer's fetcher and set the source
        // property. See feeds_file_update() how $file->feeds gets stored.
        if ($id = feeds_get_importer_id($this->config['content_type'])) {
          $class = get_class(feeds_importer($id)->fetcher);
          $target_file->feeds[$class]['source'] = $value;
          // This effectively suppresses 'import on submission' feature.
          // See feeds_file_insert().
          $target_file->feeds['suppress_import'] = TRUE;
        }
        break;
      default:
        parent::setTargetElement($source, $target_file, $target_element, $value);
        break;
    }
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();
    $targets += array(
      'fid' => array(
        'name' => t('file ID'),
        'description' => t('The fid of the file. NOTE: use this feature with care, file ids are usually assigned by Drupal.'),
        'optional_unique' => TRUE,
      ),
      'uid' => array(
        'name' => t('User ID'),
        'description' => t('The Drupal user ID of the file author.'),
      ),
      'filename' => array(
        'name' => t('File name'),
        'description' => t('File name.'),
      ),
      'uri' => array(
        'name' => t('File URI'),
        'description' => t('File URI.'),
        'callback' => 'jhu_feeds_target_file_uri',
      ),
      'filemime' => array(
        'name' => t('File mime type'),
        'description' => t('File mime type.'),
      ),
      'filesize' => array(
        'name' => t('File size'),
        'description' => t('File size.'),
      ),
      'status' => array(
        'name' => t('File status'),
        'description' => t('File status.'),
      ),
      'timestamp' => array(
        'name' => t('Upload date'),
        'description' => t('The UNIX time when a file has been uploaded.'),
      ),
    );

    // Let other modules expose mapping targets.
    self::loadMappers();
    feeds_alter('feeds_processor_targets', $targets, 'file', NULL);

    return $targets;
  }

  /**
   * Get nid of an existing feed item file if available.
   */
  protected function existingEntityId(FeedsSource $source, FeedsParserResult $result) {
    if ($fid = parent::existingEntityId($source, $result)) {
      return $fid;
    }

    // Iterate through all unique targets and test whether they do already
    // exist in the database.
    foreach ($this->uniqueTargets($source, $result) as $target => $value) {
      switch ($target) {
        case 'fid':
          $fid = db_query("SELECT fid FROM {file_managed} WHERE fid = :fid", array(':fid' => $value))->fetchField();
          break;
        case 'feeds_source':
          if ($id = feeds_get_importer_id($this->config['content_type'])) {
            $fid = db_query("SELECT fs.feed_nid FROM {file_managed} f JOIN {feeds_source} fs ON f.fid = fs.feed_nid WHERE fs.id = :id AND fs.source = :source", array(':id' => $id, ':source' => $value))->fetchField();
          }
          break;
      }
      if ($fid) {
        // Return with the first nid found.
        return $fid;
      }
    }
    return 0;
  }

  /**
   * We redefine process because we need to pass $item to $this->newEntity()
   * so we can create locally entity with the same id.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    $state = $source->state(FEEDS_PROCESS);

    while ($item = $parser_result->shiftItem()) {
      if (!($entity_id = $this->existingEntityId($source, $parser_result)) ||
           ($this->config['update_existing'] != FEEDS_SKIP_EXISTING)) {

        // Only proceed if item has actually changed.
        $hash = $this->hash($item);
        if (!empty($entity_id) && $hash == $this->getHash($entity_id)) {
          continue;
        }

        try {
          // Assemble file, map item to it, save.
          if (empty($entity_id)) {
            $entity = $this->newEntity($source, $item);
            $this->newItemInfo($entity, $source->feed_nid, $hash);
          }
          else {
            $entity = $this->entityLoad($source, $entity_id);
            // The feeds_item table is always updated with the info for the most recently processed entity.
            // The only carryover is the entity_id.
            $this->newItemInfo($entity, $source->feed_nid, $hash);
            $entity->feeds_item->entity_id = $entity_id;
          }
          $this->map($source, $parser_result, $entity);
          $this->entityValidate($entity);
          $this->entitySave($entity);

          // Track progress.
          if (empty($entity_id)) {
            $state->created++;
          }
          else {
            $state->updated++;
          }
        }
        catch (Exception $e) {
          $state->failed++;
          drupal_set_message($e->getMessage(), 'warning');
          $message = $e->getMessage();
          $message .= '<h3>Original item</h3>';
          $message .= '<pre>' . var_export($item, TRUE) . '</pre>';
          $message .= '<h3>Entity</h3>';
          $message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
          $source->log('import', $message, array(), WATCHDOG_ERROR);
        }
      }
    }

    // Set messages if we're done.
    if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
      return;
    }
    $info = $this->entityInfo();
    $tokens = array(
      '@entity' => strtolower($info['label']),
      '@entities' => strtolower($info['label plural']),
    );
    $messages = array();
    if ($state->created) {
      $messages[] = array(
       'message' => format_plural(
          $state->created,
          'Created @number @entity.',
          'Created @number @entities.',
          array('@number' => $state->created) + $tokens
        ),
      );
    }
    if ($state->updated) {
      $messages[] = array(
       'message' => format_plural(
          $state->updated,
          'Updated @number @entity.',
          'Updated @number @entities.',
          array('@number' => $state->updated) + $tokens
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => format_plural(
          $state->failed,
          'Failed importing @number @entity.',
          'Failed importing @number @entities.',
          array('@number' => $state->failed) + $tokens
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }
}
