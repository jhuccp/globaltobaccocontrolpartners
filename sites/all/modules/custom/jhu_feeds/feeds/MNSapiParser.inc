<?php

class MNSapiParser extends FeedsParser {

  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $result = new FeedsParserResult();
    $result->title = 'MNSapi Items';
    $result->description = '';
    $result->link = '';
    $fetcher_result_content = $fetcher_result->getContent();
    if (is_array($fetcher_result_content)) {
      foreach ($fetcher_result_content as $item) {
        $result->items[] = (array)$item;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($result, 'parser result');
    }
    return $result;
  }

  public function getMappingSources() {
    // Example of D5 item. We need it for keys.
    $item = (array)unserialize($this->config['serialized_example']);

    // Go though all keys and prepare items.
    $ret = array();
    foreach (array_keys($item) as $key) {
      $source = array(
        'name' => t('D5 @key', array('@key' => $key)),
        'description' => t('D5 description @key', array('@key' => $key)),
      );
      if ($key == 'body') {
        $source['callback'] = 'jhu_feeds_source_body';
      }
      $ret[$key] = $source;
    }
    return $ret + parent::getMappingSources();
  }

  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    $form['serialized_example'] = array(
      '#type' => 'textarea',
      '#title' => t('Serialized item example'),
      '#description' => t('Copy content (serialized string) of full item that you get from get call. Example copy all content you get from mn_sapi/get/node/1. This is needed to provide keys for mapping.'),
      '#default_value' => $this->config['serialized_example'],
    );
    return $form;
  }

  public function configDefaults() {
    return array(
      'serialized_example' => '',
    ) + parent::configDefaults();
  }
}
