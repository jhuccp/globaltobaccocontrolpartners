<?php

class JHUNodeFetcher extends FeedsFetcher {

  /**
   * Fetch list of source nodes.
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    // Fetch events.
    $nodes_list = unserialize(file_get_contents(variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/mn_sapi/list/node/event'));
    $nodes = array();
    if (is_array($nodes_list)) {
      $nodes_list = array_values($nodes_list);
      $i = $source_config['import_offset'];
      while ($i < $source_config['number_to_import'] + $source_config['import_offset'] && isset($nodes_list[$i])) {
        $nid = $nodes_list[$i];
        $nodes[] = unserialize(file_get_contents(variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/mn_sapi/get/node/' . $nid));
        $i++;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($nodes, 'fetcher result');
    }
//    drupal_set_message(var_export($nodes[0], TRUE));
    return new JHUFetcherResult($nodes);
  }

  public function sourceForm($source_config) {
    $form = array();
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => empty($source_config['fid']) ? 0 : $source_config['fid'],
    );
    $form['number_to_import'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of nodes to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['number_to_import']) ? 10 : $source_config['number_to_import'],
    );
    $form['import_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset of nodes to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['import_offset']) ? 0 : $source_config['import_offset'],
    );
    return $form;
  }
}
