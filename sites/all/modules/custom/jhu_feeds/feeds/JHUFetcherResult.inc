<?php

class JHUFetcherResult extends FeedsFetcherResult {
  protected $content;

  /**
   * Constructor.
   */
  public function __construct($content) {
    $this->content = $content;
  }

  public function getContent() {
    return $this->content;
  }
}
