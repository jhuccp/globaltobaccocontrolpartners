<?php

class JHUNodeProcessor extends FeedsNodeProcessor {
  /**
   * We redefine process because we need to pass $item to $this->newEntity()
   * so we can create locally entity with the same id.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    $state = $source->state(FEEDS_PROCESS);

    while ($item = $parser_result->shiftItem()) {
      if (!($entity_id = $this->existingEntityId($source, $parser_result)) ||
           ($this->config['update_existing'] != FEEDS_SKIP_EXISTING)) {

        // Only proceed if item has actually changed.
        $hash = $this->hash($item);
        if (!empty($entity_id) && $hash == $this->getHash($entity_id)) {
          continue;
        }

        try {
          // Assemble node, map item to it, save.
          if (empty($entity_id)) {
            $entity = $this->newEntity($source, $item);
            $this->newItemInfo($entity, $source->feed_nid, $hash);
          }
          else {
            $entity = $this->entityLoad($source, $entity_id);
            // The feeds_item table is always updated with the info for the most recently processed entity.
            // The only carryover is the entity_id.
            $this->newItemInfo($entity, $source->feed_nid, $hash);
            $entity->feeds_item->entity_id = $entity_id;
          }
          $this->map($source, $parser_result, $entity);
          $this->entityValidate($entity);
          $this->entitySave($entity);

          // Set changed timestamp properly.
          db_query('UPDATE {node} SET changed = :changed WHERE nid = :nid', array(':changed' => $item['changed'], ':nid' => $entity->nid));
          db_query('UPDATE {node_revision} SET timestamp = :changed WHERE nid = :nid AND vid = :vid',
            array(':changed' => $item['changed'], ':nid' => $entity->nid, ':vid' => $entity->vid));

          // Track progress.
          if (empty($entity_id)) {
            $state->created++;
          }
          else {
            $state->updated++;
          }
        }
        catch (Exception $e) {
          $state->failed++;
          drupal_set_message($e->getMessage(), 'warning');
          $message = $e->getMessage();
//          $message .= '<h3>Original item</h3>';
//          $message .= '<pre>' . var_export($item, TRUE) . '</pre>';
//          $message .= '<h3>Entity</h3>';
//          $message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
          $source->log('import', $message, array(), WATCHDOG_ERROR);
        }
      }
    }

    // Set messages if we're done.
    if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
      return;
    }
    $info = $this->entityInfo();
    $tokens = array(
      '@entity' => strtolower($info['label']),
      '@entities' => strtolower($info['label plural']),
    );
    $messages = array();
    if ($state->created) {
      $messages[] = array(
       'message' => format_plural(
          $state->created,
          'Created @number @entity.',
          'Created @number @entities.',
          array('@number' => $state->created) + $tokens
        ),
      );
    }
    if ($state->updated) {
      $messages[] = array(
       'message' => format_plural(
          $state->updated,
          'Updated @number @entity.',
          'Updated @number @entities.',
          array('@number' => $state->updated) + $tokens
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => format_plural(
          $state->failed,
          'Failed importing @number @entity.',
          'Failed importing @number @entities.',
          array('@number' => $state->failed) + $tokens
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }

  public function getMappingTargets() {
    $type = node_type_get_type($this->config['content_type']);
    $targets = parent::getMappingTargets();
    if ($type->has_title) {
      $targets['title'] = array(
        'name' => t('Title'),
        'description' => t('The title of the node.'),
      );
    }
    $targets += array(
      'nid' => array(
        'name' => t('Node ID'),
        'description' => t('The nid of the node. NOTE: use this feature with care, node ids are usually assigned by Drupal.'),
        'optional_unique' => TRUE,
      ),
      'vid' => array(
        'name' => t('Node Version ID'),
        'description' => t('The vid of the node. NOTE: use this feature with care, node ids are usually assigned by Drupal.'),
        'optional_unique' => TRUE,
      ),
      'uid' => array(
        'name' => t('User ID'),
        'description' => t('The Drupal user ID of the node author.'),
      ),
      'status' => array(
        'name' => t('Published status'),
        'description' => t('Whether a node is published or not. 1 stands for published, 0 for not published.'),
      ),
      'created' => array(
        'name' => t('Created date'),
        'description' => t('The UNIX time when a node has been published.'),
      ),
      'changed' => array(
        'name' => t('Changed date'),
        'description' => t('The UNIX time when a node has been updated last time.'),
      ),
      'event_date' => array(
        'name' => t('Event date'),
        'description' => t('Custom event date field importer.'),
        'callback' => 'jhu_feeds_target_event_date_field'
      ),
    );

    // If the target content type is a Feed node, expose its source field.
    if ($id = feeds_get_importer_id($this->config['content_type'])) {
      $name = feeds_importer($id)->config['name'];
      $targets['feeds_source'] = array(
        'name' => t('Feed source'),
        'description' => t('The content type created by this processor is a Feed Node, it represents a source itself. Depending on the fetcher selected on the importer "@importer", this field is expected to be for example a URL or a path to a file.', array('@importer' => $name)),
        'optional_unique' => TRUE,
      );
    }

    // Let other modules expose mapping targets.
    self::loadMappers();
    feeds_alter('feeds_processor_targets', $targets, 'node', $this->config['content_type']);

    // Set special set callback for body.
    $targets['body']['callback'] = 'jhu_feeds_target_text_field';
    // Set special set callback for filefields
    $targets['field_event_draftagenda']['callback'] = 'jhu_feeds_target_file_field';
    $targets['field_event_eventdocuments']['callback'] = 'jhu_feeds_target_file_field';
    $targets['field_event_invitedparticipants']['callback'] = 'jhu_feeds_target_file_field';

    // Change taxonomy field setter.
    foreach ($targets as &$target) {
      if (isset($target['callback']) && $target['callback'] == 'taxonomy_feeds_set_target') {
        $target['callback'] = 'jhu_feeds_target_taxonomy';
      }
    }

    return $targets;
  }

  /**
   * Manually create records in node table.
   */
  protected function newEntity(FeedsSource $source) {
    $d5node = func_get_arg(1);

    db_query('REPLACE INTO {node} (nid, vid, type) VALUES (:nid, :vid, :type)', array(':nid' => $d5node['nid'], ':vid' => $d5node['vid'], ':type' => 'event'));
    db_query('REPLACE INTO {node_revision} (nid, vid, log) VALUES (:nid, :vid, :log)', array(':nid' => $d5node['nid'], ':vid' => $d5node['vid'], ':log' => ''));
    // node_comments_statistics
    db_query('REPLACE INTO {node_comment_statistics} (nid) VALUES (:nid)', array(':nid' => $d5node['nid']));

    $node = new stdClass();
    $node->type = $this->config['content_type'];
    $node->changed = REQUEST_TIME;
    $node->created = REQUEST_TIME;
    $node->language = LANGUAGE_NONE;
    $node->comment_count = 0;
    node_object_prepare($node);
    // Populate properties that are set by node_object_prepare().
    $node->log = 'Created by FeedsNodeProcessor';
    $node->uid = $this->config['author'];
    return $node;
  }
}
