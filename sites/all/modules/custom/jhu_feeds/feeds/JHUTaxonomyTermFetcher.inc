<?php

class JHUTaxonomyTermFetcher extends FeedsFetcher {

  /**
   * Fetch list of source nodes.
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    // Fetch events.
    $items_list = unserialize(file_get_contents(variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/mn_sapi/list/term'));
    $items = array();
    if (is_array($items_list)) {
      $items_list = array_values($items_list);
      $i = $source_config['import_offset'];
      while ($i < $source_config['number_to_import'] + $source_config['import_offset'] && isset($items_list[$i])) {
        $item_id = $items_list[$i];
        $url = variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/mn_sapi/get/term/' . $item_id;
        $content = file_get_contents($url);
        $item = unserialize($content);
        $items[] = $item;
        $i++;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($items, 'fetcher result');
    }
//    drupal_set_message(var_export($items[0], TRUE));
    return new JHUFetcherResult($items);
  }

  public function sourceForm($source_config) {
    $form = array();
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => empty($source_config['fid']) ? 0 : $source_config['fid'],
    );
    $form['number_to_import'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of items to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['number_to_import']) ? 10 : $source_config['number_to_import'],
    );
    $form['import_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset of items to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['import_offset']) ? 0 : $source_config['import_offset'],
    );
    return $form;
  }
}
