<?php

class MNSapiFetcher extends FeedsFetcher {

  /**
   * Fetch list of source nodes.
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    // Fetch events.
    $items_list = unserialize(file_get_contents(variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/' . $this->config['list_url']));
    $items = array();
    if (is_array($items_list)) {
      $items_list = array_values($items_list);
      $i = $source_config['import_offset'];
      while ($i < $source_config['number_to_import'] + $source_config['import_offset'] && isset($items_list[$i])) {
        $url = variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/' . $this->config['get_url'];
        $url = str_replace('[id]', $items_list[$i], $url);
        $items[] = unserialize(file_get_contents($url));
        $i++;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($items, 'fetcher result');
    }
    return new JHUFetcherResult($items);
  }

  public function sourceForm($source_config) {
    $form = array();
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => empty($source_config['fid']) ? 0 : $source_config['fid'],
    );
    $form['number_to_import'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of items to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['number_to_import']) ? 10 : $source_config['number_to_import'],
    );
    $form['import_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset of items to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['import_offset']) ? 0 : $source_config['import_offset'],
    );
    return $form;
  }

  /**
   * Override parent::configForm().
   */
  public function configForm(&$form_state) {
    $form = parent::configForm($form_state);
    $form['list_url'] = array(
      '#type' => 'textfield',
      '#title' => t('List URL'),
      '#description' => t('URL of the call to get the list of items. Example: mn_sapi/list/node'),
      '#default_value' => $this->config['list_url'],
    );
    $form['get_url'] = array(
      '#type' => 'textfield',
      '#title' => t('Get URL'),
      '#description' => t('URL of the call to get the full details about individual item. Example: mn_sapi/get/node/[id]. Token to substitute with id is [id].'),
      '#default_value' => $this->config['get_url'],
    );
    return $form;
  }

  public function configDefaults() {
    return array(
      'list_url' => '',
      'get_url' => '',
    ) + parent::configDefaults();
  }
}
