<?php

class JHUUserProcessor extends FeedsUserProcessor {
  /**
   * We redefine process because we need to pass $item to $this->newEntity()
   * so we can create locally entity with the same id.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    $state = $source->state(FEEDS_PROCESS);

    $processed_entities = array();

    while ($item = $parser_result->shiftItem()) {
      if (!($entity_id = $this->existingEntityId($source, $parser_result)) ||
           ($this->config['update_existing'] != FEEDS_SKIP_EXISTING)) {

        // Do not import user 1.
        if ($entity_id == 1) {
          continue;
        }

        // Only proceed if item has actually changed.
        $hash = $this->hash($item);
        if (!empty($entity_id) && $hash == $this->getHash($entity_id)) {
          continue;
        }

        // Do not send notifications to imported users. We will restore this
        // variable later.
        $email_notify = variable_get('user_mail_status_activated_notify', TRUE);
        variable_set('user_mail_status_activated_notify', FALSE);

        try {
          // Assemble node, map item to it, save.
          if (empty($entity_id)) {
            $entity = $this->newEntity($source, $item);
            $this->newItemInfo($entity, $source->feed_nid, $hash);
          }
          else {
            $entity = $this->entityLoad($source, $entity_id);
            // The feeds_item table is always updated with the info for the most recently processed entity.
            // The only carryover is the entity_id.
            $this->newItemInfo($entity, $source->feed_nid, $hash);
            $entity->feeds_item->entity_id = $entity_id;
          }
          $this->map($source, $parser_result, $entity);
          $this->entityValidate($entity);

          $entity_clone = clone $entity;
          $this->entitySave($entity);

          // Update password as now it is double hashed.
          if (isset($entity_clone->pass)) {
            db_query('UPDATE {users} SET pass = :pass WHERE uid = :uid', array(':pass' => $entity_clone->pass, ':uid' => $entity_clone->uid));
          }

          $processed_entities[] = $entity;
          // Track progress.
          if (empty($entity_id)) {
            $state->created++;
          }
          else {
            $state->updated++;
          }
        }
        catch (Exception $e) {
          $state->failed++;
          drupal_set_message($e->getMessage(), 'warning');
          $message = $e->getMessage();
          $message .= '<h3>Original item</h3>';
          $message .= '<pre>' . var_export($item, TRUE) . '</pre>';
          $message .= '<h3>Entity</h3>';
          $message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
          $source->log('import', $message, array(), WATCHDOG_ERROR);
        }

        // Restore email notify value.
        variable_set('user_mail_status_activated_notify', $email_notify);

        if (variable_get('jhu_feeds_debug', FALSE)) {
          dpm($processed_entities, 'processor result');
        }
      }
    }

    // Set messages if we're done.
    if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
      return;
    }
    $info = $this->entityInfo();
    $tokens = array(
      '@entity' => strtolower($info['label']),
      '@entities' => strtolower($info['label plural']),
    );
    $messages = array();
    if ($state->created) {
      $messages[] = array(
       'message' => format_plural(
          $state->created,
          'Created @number @entity.',
          'Created @number @entities.',
          array('@number' => $state->created) + $tokens
        ),
      );
    }
    if ($state->updated) {
      $messages[] = array(
       'message' => format_plural(
          $state->updated,
          'Updated @number @entity.',
          'Updated @number @entities.',
          array('@number' => $state->updated) + $tokens
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => format_plural(
          $state->failed,
          'Failed importing @number @entity.',
          'Failed importing @number @entities.',
          array('@number' => $state->failed) + $tokens
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }

  /**
   * Manually create records in node table.
   */
  protected function newEntity(FeedsSource $source) {
    $item = func_get_arg(1);

    db_query('REPLACE INTO {users} (uid, name, pass) VALUES (:uid, :name, :pass)', array(':uid' => $item['uid'], ':name' => $item['name'], ':pass' => $item['pass']));

    $account = new stdClass();
    $account->uid = 0;
    $account->roles = array_filter($this->config['roles']);
    $account->status = $this->config['status'];
    $account->timezone = NULL;
    return $account;
  }

  /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();
    $targets += array(
      'uid' => array(
        'name' => t('User ID'),
        'description' => t('Name of the user.'),
        'optional_unique' => TRUE,
       ),
      'name' => array(
        'name' => t('User name'),
        'description' => t('Name of the user.'),
        'optional_unique' => TRUE,
       ),
      'mail' => array(
        'name' => t('Email address'),
        'description' => t('Email address of the user.'),
        'optional_unique' => TRUE,
       ),
      'created' => array(
        'name' => t('Created date'),
        'description' => t('The created (e. g. joined) data of the user.'),
       ),
      'access' => array(
        'name' => t('Access date'),
        'description' => t('Last time user login.'),
       ),
      'status' => array(
        'name' => t('Status'),
        'description' => t('User status.'),
       ),
      'timezone' => array(
        'name' => t('Timezone'),
        'description' => t('Timezone.'),
        'callback' => 'jhu_feeds_target_user_timezone'
       ),
      'roles' => array(
        'name' => t('Roles'),
        'description' => t('Roles.'),
        'callback' => 'jhu_feeds_target_user_roles'
       ),
    );

    $targets['pass'] = array(
      'name' => t('D5 Password'),
      'description' => t('The user password.'),
      'callback' => 'jhu_feeds_target_user_password'
    );

    // Let other modules expose mapping targets.
    self::loadMappers();
    feeds_alter('feeds_processor_targets', $targets, 'user', 'user');
    $targets['field_user_picture']['callback'] = 'jhu_feeds_target_file_field';

    return $targets;
  }
}
