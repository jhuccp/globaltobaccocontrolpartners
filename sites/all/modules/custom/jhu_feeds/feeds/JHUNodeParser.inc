<?php

class JHUNodeParser extends FeedsParser {

  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $result = new FeedsParserResult();
    $result->title = 'Events';
    $result->description = 'Events D5 nodes';
    $result->link = '';
    $fetcher_result_content = $fetcher_result->getContent();
    if (is_array($fetcher_result_content)) {
      foreach ($fetcher_result_content as $d5node) {
//        // Enforce import for testing purposes. Hash will be different all the time.
//        $d5node->title = $d5node->title, 0, 20) . rand();
        $result->items[] = (array)$d5node;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($result, 'parser result');
    }
    return $result;
  }

  public function getMappingSources() {
    $sources = array(
      'nid' => array(
        'name' => t('Nid'),
        'description' => t('Nid of the D5 node.'),
      ),
      'vid' => array(
        'name' => t('Vid'),
        'description' => t('Vid of the D5 node.'),
      ),
      'type' => array(
        'name' => t('Type'),
        'description' => t('Node type of the D5 node.'),
      ),
      'created' => array(
        'name' => t('Created'),
        'description' => t('Created timestamp of the D5 node.'),
      ),
      'changed' => array(
        'name' => t('Changed'),
        'description' => t('Changed timestamp of the D5 node.'),
      ),
      'comment' => array(
        'name' => t('Comment field'),
        'description' => t('Comments status (opened/closed) of the D5 node.'),
      ),
      'promote' => array(
        'name' => t('Promote'),
        'description' => t('Promote to frontpage status of the D5 node.'),
      ),
      'sticky' => array(
        'name' => t('Sticky'),
        'description' => t('Sticky status of the D5 node.'),
      ),
      'revision_timestamp' => array(
        'name' => t('Revision timestamp'),
        'description' => t('Revision timestamp of the D5 node.'),
      ),
      'title' => array(
        'name' => t('Title'),
        'description' => t('Title of the D5 node.'),
      ),
      'body' => array(
        'name' => t('Body'),
        'description' => t('Body of the D5 node.'),
        'callback' => 'jhu_feeds_source_body',
      ),
      'teaser' => array(
        'name' => t('Teaser'),
        'description' => t('Teaser of the D5 node.'),
      ),
      'log' => array(
        'name' => t('Log message'),
        'description' => t('Log message of the D5 node.'),
      ),
      'format' => array(
        'name' => t('Body fomat'),
        'description' => t('Body fomat of the D5 node.'),
      ),
      'uid' => array(
        'name' => t('Uid'),
        'description' => t('Uid of author of the D5 node.'),
      ),
      'field_event_submitted_by' => array(
        'name' => t('Submitted by'),
        'description' => t('CCK field "Submitted by".'),
        'callback' => 'jhu_feeds_source_cck_field',
      ),
      'date' => array(
        'name' => t('Event Date'),
        'description' => t('Date provided by Event module.'),
        'callback' => 'jhu_feeds_source_date_event',
      ),
    ) + parent::getMappingSources();

    $example_node = array(
      'nid' => '1217',
      'vid' => '1242',
      'type' => 'event',
      'status' => '1',
      'created' => '1207562362',
      'changed' => '1207562362',
      'comment' => '2',
      'promote' => '1',
      'sticky' => '0',
      'revision_timestamp' => '1207562362',
      'title' => 'COP3',
      'body' => 'The 3rd meeting of the Conference of the Parties ',
      'teaser' => 'The 3rd meeting of the Conference of the Parties ',
      'log' => '',
      'format' => '1',
      'uid' => '89',
      'name' => 'ellierampton',
      'picture' => '',
      'data' => 'a:7:{s:7:"contact";i:1;s:19:"mass_contact_optout";i:0;s:16:"privatemsg_allow";i:1;s:28:"privatemsg_setmessage_notify";i:1;s:20:"privatemsg_mailalert";s:1:"0";s:14:"picture_delete";i:0;s:14:"picture_upload";s:0:"";}',
      'print_display' => 1,
      'print_display_comment' => 0,
      'print_display_urllist' => 1,
      'last_comment_timestamp' => '1207562362',
      'last_comment_name' => NULL,
      'comment_count' => '0',
      'taxonomy' => array(
          385 => array(array( 'tid' => '385', 'vid' => '16', 'name' => 'Onsite Meeting', 'description' => '', 'weight' => '0' )),
          115 => array(array( 'tid' => '115', 'vid' => '5', 'name' => 'Non-specific', 'description' => '', 'weight' => '-10', )),
          114 => array(array( 'tid' => '114', 'vid' => '11', 'name' => 'Non-specific', 'description' => '', 'weight' => '-9', )),
          348 => array(array( 'tid' => '348', 'vid' => '12', 'name' => 'Framework Convention on Tobacco Control (FCTC)', 'description' => '', 'weight' => '0', )), ),
      'field_event_submitted_by' => array ( 0 => array ( 'value' => 'Ellie Rampton', ), ),
      'field_location' => array ( 0 => array ( 'value' => 'Santon, Johannesberg', ), ),
      'field_provisional' => array ( 0 => array ( 'value' => NULL, ), ),
      'field_contact' => array ( 0 => array ( 'value' => NULL, ), ),
      'field_draft_agenda' => array ( 0 => array ( 'fid' => NULL, ), ),
      'field_working_documents' => array ( 0 => array ( 'fid' => NULL, ), ),
      'field_invited_participants' => array ( 0 => array ( 'fid' => NULL, ), ),
      'event_start' => '1226901600',
      'event_end' => '1227348000',
      'timezone' => '20',
      'start_offset' => '+21600',
      'start_format' => '11/17/2008 - 12:00pm',
      'start_time_format' => '12:00 pm',
      'end_offset' => '+21600',
      'end_format' => '11/22/2008 - 4:00pm',
      'end_time_format' => '4:00 pm',
      'event_node_title' => 'event',
    );
    // Add remaining items.
    foreach (array_diff(array_keys($example_node), array_keys($sources)) as $key) {
      $sources[$key] = array(
        'name' => 'D5 ' . $key,
        'description' => $key,
      );
    }

    // Use custom source callback for CCK fields.
    $sources['field_provisional']['callback'] = 'jhu_feeds_source_cck_field';
    $sources['field_contact']['callback'] = 'jhu_feeds_source_cck_field';
    $sources['field_draft_agenda']['callback'] = 'jhu_feeds_source_cck_file_field';
    $sources['field_working_documents']['callback'] = 'jhu_feeds_source_cck_file_field';
    $sources['field_invited_participants']['callback'] = 'jhu_feeds_source_cck_file_field';

    return $sources;
  }
}
