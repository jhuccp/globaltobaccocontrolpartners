<?php

class JHUUserParser extends FeedsParser {

  public function parse(FeedsSource $source, FeedsFetcherResult $fetcher_result) {
    $result = new FeedsParserResult();
    $result->title = 'Users';
    $result->description = 'D5 Users';
    $result->link = '';
    $fetcher_result_content = $fetcher_result->getContent();
    if (is_array($fetcher_result_content)) {
      foreach ($fetcher_result_content as $d5user) {
        $result->items[] = (array)$d5user;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($result, 'parser result');
    }
    return $result;
  }

  public function getMappingSources() {
    // Example of D5 user. We need it for keys.
    $d5_user = array(
      'uid' => '11',
      'name' => 'test',
      'pass' => '098f6bcd4621d373cade4e832627b4f6',
      'mail' => 'test@testestestest123.com',
      'mode' => '0',
      'sort' => '0',
      'threshold' => '0',
      'theme' => '',
      'signature' => '',
      'created' => '1185468209',
      'access' => '1206978946',
      'login' => '1206978876',
      'status' => '1',
      'timezone' => '-14400',
      'language' => '',
      'picture' => '',
      'init' => 'gchalk@gmail.com',
      'data' => 'a:8:{s:7:"contact";i:1;s:17:"mimemail_textonly";i:0;s:8:"og_email";s:1:"2";s:14:"picture_delete";i:0;s:14:"picture_upload";s:0:"";s:5:"block";a:1:{s:4:"poll";a:1:{i:0;i:0;}}s:10:"skype_name";s:0:"";s:22:"available_skype_status";a:1:{i:7;s:1:"7";}}',
      'contact' => 1,
      'mimemail_textonly' => 0,
      'og_email' => '2',
      'picture_delete' => 0,
      'picture_upload' => '',
      'block' => array ( 'poll' => array ( 0 => 0, ), ),
      'skype_name' => '',
      'available_skype_status' => array ( 7 => '7', ),
      'roles' => array ( 2 => 'authenticated user', ),
      'profile_title' => '',
      'profile_firstname' => 'Test',
      'profile_lastname' => 'User',
      'profile_phonenumber' => '',
      'profile_city' => '',
      'profile_country' => '',
      'profile_previousEd' => '',  // Bio
      'profile_proInterests' => '',
      'profile_partner_org' => '', // Partner Organization
      'profile_faxnumber' => '',
      'profile_address' => '',
      'profile_bprojects' => '', // Bloomberg Projects
    );

    // Go though all keys and prepare items.
    $ret = array();
    foreach (array_keys($d5_user) as $key) {
      $ret[$key] = array(
        'name' => t('D5 @key', array('@key' => $key)),
        'description' => t('D5 description @key', array('@key' => $key)),
      );
    }
    return $ret + parent::getMappingSources();
  }
}
