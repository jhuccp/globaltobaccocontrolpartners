<?php

class JHUUserFetcher extends FeedsFetcher {

  /**
   * Fetch list of source nodes.
   */
  public function fetch(FeedsSource $source) {
    $source_config = $source->getConfigFor($this);
    // Fetch events.
    $users_list = unserialize(file_get_contents(variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/mn_sapi/list/user'));
    $users = array();
    if (is_array($users_list)) {
      $users_list = array_values($users_list);
      $i = $source_config['import_offset'];
      while ($i < $source_config['number_to_import'] + $source_config['import_offset'] && isset($users_list[$i])) {
        $uid = $users_list[$i];

        // Do not import user 0 and 1.
        if ($uid > 1) {
          $users[] = unserialize(file_get_contents(variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners') . '/mn_sapi/get/user/' . $uid));
        }
        $i++;
      }
    }
    if (variable_get('jhu_feeds_debug', FALSE)) {
      dpm($users, 'fetcher result');
    }
//    drupal_set_message(var_export($users[0], TRUE));
    return new JHUFetcherResult($users);
  }

  public function sourceForm($source_config) {
    $form = array();
    $form['fid'] = array(
      '#type' => 'value',
      '#value' => empty($source_config['fid']) ? 0 : $source_config['fid'],
    );
    $form['number_to_import'] = array(
      '#type' => 'textfield',
      '#title' => t('Number of users to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['number_to_import']) ? 10 : $source_config['number_to_import'],
    );
    $form['import_offset'] = array(
      '#type' => 'textfield',
      '#title' => t('Offset of users to import'),
      '#size' => 10,
      '#default_value' => empty($source_config['import_offset']) ? 0 : $source_config['import_offset'],
    );
    return $form;
  }
}
