<?php

class JHUTaxonomyTermProcessor extends FeedsTermProcessor {
  /**
   * We redefine process because we need to pass $item to $this->newEntity()
   * so we can create locally entity with the same id.
   */
  public function process(FeedsSource $source, FeedsParserResult $parser_result) {
    $state = $source->state(FEEDS_PROCESS);

    while ($item = $parser_result->shiftItem()) {
      if (!($entity_id = $this->existingEntityId($source, $parser_result)) ||
           ($this->config['update_existing'] != FEEDS_SKIP_EXISTING)) {

        // Only proceed if item has actually changed.
        $hash = $this->hash($item);
        if (!empty($entity_id) && $hash == $this->getHash($entity_id)) {
          continue;
        }

        try {
          // Assemble node, map item to it, save.
          if (empty($entity_id)) {
            $entity = $this->newEntity($source, $item);
            $this->newItemInfo($entity, $source->feed_nid, $hash);
          }
          else {
            $entity = $this->entityLoad($source, $entity_id);
            // The feeds_item table is always updated with the info for the most recently processed entity.
            // The only carryover is the entity_id.
            $this->newItemInfo($entity, $source->feed_nid, $hash);
            $entity->feeds_item->entity_id = $entity_id;
          }
          $this->map($source, $parser_result, $entity);
          $this->entityValidate($entity);
          $this->entitySave($entity);

          // Track progress.
          if (empty($entity_id)) {
            $state->created++;
          }
          else {
            $state->updated++;
          }
        }
        catch (Exception $e) {
          $state->failed++;
          drupal_set_message($e->getMessage(), 'warning');
          $message = $e->getMessage();
          $message .= '<h3>Original item</h3>';
          $message .= '<pre>' . var_export($item, TRUE) . '</pre>';
          $message .= '<h3>Entity</h3>';
          $message .= '<pre>' . var_export($entity, TRUE) . '</pre>';
          $source->log('import', $message, array(), WATCHDOG_ERROR);
        }
      }
    }

    // Set messages if we're done.
    if ($source->progressImporting() != FEEDS_BATCH_COMPLETE) {
      return;
    }
    $info = $this->entityInfo();
    $tokens = array(
      '@entity' => strtolower($info['label']),
      '@entities' => strtolower($info['label plural']),
    );
    $messages = array();
    if ($state->created) {
      $messages[] = array(
       'message' => format_plural(
          $state->created,
          'Created @number @entity.',
          'Created @number @entities.',
          array('@number' => $state->created) + $tokens
        ),
      );
    }
    if ($state->updated) {
      $messages[] = array(
       'message' => format_plural(
          $state->updated,
          'Updated @number @entity.',
          'Updated @number @entities.',
          array('@number' => $state->updated) + $tokens
        ),
      );
    }
    if ($state->failed) {
      $messages[] = array(
       'message' => format_plural(
          $state->failed,
          'Failed importing @number @entity.',
          'Failed importing @number @entities.',
          array('@number' => $state->failed) + $tokens
        ),
        'level' => WATCHDOG_ERROR,
      );
    }
    if (empty($messages)) {
      $messages[] = array(
        'message' => t('There are no new @entities.', array('@entities' => strtolower($info['label plural']))),
      );
    }
    foreach ($messages as $message) {
      drupal_set_message($message['message']);
      $source->log('import', $message['message'], array(), isset($message['level']) ? $message['level'] : WATCHDOG_INFO);
    }
  }

  /**
   * Creates a new term in memory and returns it.
   */
  protected function newEntity(FeedsSource $source) {
    $item = func_get_arg(1);

    // Set parent of the term.
    $parent = 0;
    if (!empty($item['parents'])) {
      $parent_term = current($item['parents']);
      $parent = (int)$parent_term->tid;
    }

    db_query('REPLACE INTO {taxonomy_term_data} (tid, vid, name) VALUES (:tid, :vid, :name)', array(':tid' => $item['tid'], ':vid' => $item['vid'], ':name' => $item['name']));

    db_query('DELETE FROM {taxonomy_term_hierarchy} WHERE tid = :tid', array(':tid' => $item['tid']));
    db_query('REPLACE INTO {taxonomy_term_hierarchy} (tid, parent) VALUES (:tid, :parent)', array(':tid' => $item['tid'], ':parent' => $parent));

    $vocabulary = $this->vocabulary();
    $term = new stdClass();
    $term->tid = $item['tid'];
    $term->vid = $item['vid'];
    $term->vocabulary_machine_name = $vocabulary->machine_name;
    return $term;
  }

    /**
   * Return available mapping targets.
   */
  public function getMappingTargets() {
    $targets = parent::getMappingTargets();
    $targets += array(
      'name' => array(
        'name' => t('Term name'),
        'description' => t('Name of the taxonomy term.'),
        'optional_unique' => TRUE,
       ),
      'description' => array(
        'name' => t('Term description'),
        'description' => t('Description of the taxonomy term.'),
       ),
      'tid' => array(
        'name' => t('Term ID'),
        'description' => t('ID of the taxonomy term.'),
        'optional_unique' => TRUE,
       ),
      'vid' => array(
        'name' => t('Term VID'),
        'description' => t('Vocabulary ID of the taxonomy term.'),
       ),
      'parents' => array(
        'name' => t('Term Parents'),
        'description' => t('Parent terms of the taxonomy term.'),
       ),

    );
    // Let implementers of hook_feeds_term_processor_targets() add their targets.
    try {
      self::loadMappers();
      feeds_alter('feeds_processor_targets', $targets, 'taxonomy_term', $this->vocabulary()->machine_name);
    }
    catch (Exception $e) {
      // Do nothing.
    }
    return $targets;
  }

}