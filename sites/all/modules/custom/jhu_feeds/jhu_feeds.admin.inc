<?php

/**
 * Settings form.
 */
function jhu_feeds_admin_settings() {
  $form = array();
  $form['jhu_feeds_url'] = array(
    '#title' => t('URL for import feed'),
    '#type' => 'textfield',
    '#description' => t('Site where mn_sapi is enabled. Example: http://clients.trellon.org/jhutobaccopartners'),
    '#default_value' => variable_get('jhu_feeds_url', 'http://clients.trellon.org/jhutobaccopartners'),
  );
  $form['jhu_feeds_debug'] = array(
    '#title' => t('Debug mode'),
    '#type' => 'checkbox',
    '#description' => t('You will see how items fetched and processed'),
    '#default_value' => variable_get('jhu_feeds_debug', FALSE),
  );
  return system_settings_form($form);
}
