<?php

/**
 * Implements hook_feeds_processor_targets_alter().
 */
function jhu_feeds_feeds_processor_targets_alter(&$targets, $entity_type, $entity_bundle) {
  if ($entity_type != 'node') {
    return;
  }

  switch ($entity_bundle) {
    case 'country':
      $targets['field_country_statistics']['callback'] = 'jhu_feeds_target_text_field';
      break;
    case 'partner':
      $targets['field_partner_organization']['callback'] = 'jhu_feeds_target_text_field';
      break;
  }
}
