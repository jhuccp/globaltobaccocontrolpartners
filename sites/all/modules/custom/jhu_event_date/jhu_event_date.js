(function ($) {

/**
 * Attaches the event date behavior to all timezone select form elements.
 */

/*
Drupal.behaviors.jhu_event_date = {
  attach: function (context, settings) {
    $('.field-name-field-event-date .form-item-timezone-select select:not(.jhu-event-date-processed)', context).each(function(){
      $(this).parent().parent().parent().show();
      $(this).addClass('jhu-event-date-processed').change(function(){
        var id = this.id;
        id = id.replace(/-/gi, '_');

        var value = this.value;
        var text = $(this).find('option:selected').first().text();
        var wrapper = $(this).parent().parent().parent().parent();

        $(wrapper).find('.field-name-field-event-date-start-date .field-item').html(Drupal.settings[id][value]['start']);
        $(wrapper).find('.field-name-field-event-date-end-date .field-item').html(Drupal.settings[id][value]['end']);
        $(wrapper).find('.field-name-field-event-date-timezone .field-item').html(text);
      });
    });

  }
};
*/

})(jQuery);
