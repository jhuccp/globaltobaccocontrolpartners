<?php

/**
 * @file mass_contact plugin type groupping_method.
 *
 * Select users by taxonomy terms.
 */

$plugin = array(
  'create_recipient_list_callback' => 'mass_contact_og_create_recipient_list',
  'mass_contact_admin_categories_callback' => 'mass_contact_og_admin_categories',
  'mass_contact_admin_edit' => 'mass_contact_og_admin_edit',
  'mass_contact_admin_edit_validate' => 'mass_contact_og_admin_edit_validate',
  'mass_contact_admin_edit_submit' => 'mass_contact_og_admin_edit_submit',
);

/**
 * Callback to retrieve users by taxonomy terms.
 *
 * Get the user IDs for all the users that have specified taxonomy terms
 * attached to user object.
 */
function mass_contact_og_create_recipient_list($recipients) {
  $ids = array();
  if (!isset($recipients['jhu_mass_contact_og']) || empty($recipients['jhu_mass_contact_og'])) {
    return array();
  }

  $ids = $recipients['jhu_mass_contact_og'];

  $ogms = db_select('og_membership', 'ogm')
    ->fields('ogm')
    ->condition('gid', $ids, 'IN')
    ->condition('entity_type', 'user')
    ->execute();

  if (empty($ogms)) {
    return array();
  }

  // Collect uids.
  $uids = array();
  foreach ($ogms as $ogm) {
    $uids[] = $ogm->etid;
  }

  return $uids;
}

/**
 * Callback to prepare taxonomy term names for admin/config/system/mass_contact
 * page column Recipients.
 */
function mass_contact_og_admin_categories($recipients) {
  $ids = array();
  if (!isset($recipients['jhu_mass_contact_og']) || empty($recipients['jhu_mass_contact_og'])) {
    return;
  }
  $ids = $recipients['jhu_mass_contact_og'];

  $ogs = db_select('og', 'og')
    ->fields('og')
    ->condition('gid', $ids, 'IN')
    ->execute();

  if (empty($ogs)) {
    return;
  }

  $og_names = array();
  foreach ($ogs as $og) {
    $og_names[] = $og->label;
  }

  return t('Ogranic groups: %ogs', array('%ogs' => implode(', ', $og_names)));
}

/**
 * Form element for Category add/edit page.
 *
 * @param array $terms
 * @return type
 */
function mass_contact_og_admin_edit($recipients) {
  $form_element = array();

  $edit = db_select('og', 'og')
    ->fields('og')
    ->execute();

  $options = array();
  foreach ($edit as $og => $fields) {
    $options[$fields->gid] = check_plain($fields->label);
  }

  $default_value = array();
  if (isset($recipients['jhu_mass_contact_og'])) {
    $default_value = $recipients['jhu_mass_contact_og'];
  }

  $form_element['mass_contact_og'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Organic groups:'),
    '#options' => $options,
    '#default_value' => $default_value,
  );

  return $form_element;
}

/**
 * Add/edit validation callback. Set form error and return whether selection
 * is empty or not.
 *
 * @param array $form
 * @param array $form_state
 * @return bool
 */
function mass_contact_og_admin_edit_validate($form, &$form_state) {
  foreach ($form_state['values']['recipients']['jhu_mass_contact_og'] as $og_values) {
    $og_values_filtered = array_filter($og_values);
    if (!empty($og_values_filtered)) {
      return FALSE;
    }
  }
  return TRUE;
}

/**
 * Add/edit form submit callback. Should return piece of data that will be
 * saved to mass_contact table in recepients field.
 *
 * @param type $form
 * @param type $form_state
 */
function mass_contact_og_admin_edit_submit($form, &$form_state) {
  $ogs = array();
  foreach ($form_state['values']['recipients']['jhu_mass_contact_og'] as $og_values) {
    $ogs += array_filter($og_values);
  }
  return $ogs;
}